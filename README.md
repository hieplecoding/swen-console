# swen-console
https://gitlab.com/hieplecoding/swen-console


## Getting started
-> means need to do

started the project: 12.11
12.11:

create the console project
create class and field
think about inheritance and interface

27.11
test a simple game in the main program.cs (Battle logic)
- update the attack method to a tuple method
- write some n Test
- finish the simple game and run in the program.cs 

1.12:
build http server, implement http methods for user and connect the database (PostgreSQL):
- send test request to server
- install PostgreSQL image on Docker
- run and connect to PostgreSQL database named mydatabase
User:
- implement the post request /users: register new user
- implement the get request /users/{username}: Retrieves the user data for the username

2.12:
- move whole project to rider, not need parallel

4.12:
- update addUser to able to insert informations for UserData
put: /users/{username}
- implement the put methods to update the UserData
session: /sessions
- login with correct username and password

5.12:
- finished sessions

6.12:
- implement integration tests for users
- create new test database and table : testmydatabase and testusers
- have problem with the config file to allow use 2 database -> decide insert the test user inside the production database -_-

8.12:
- implement the add card to package mechanism 
- create table to receive card in the package ...
- not sure how to implement that.
- an approach to save the login state is jwt token
- username: admin,password: admin.
- create table two table: packages and card, card has the foreign key that is the primary key of the packages

-> implement test for package

12.12:
- implement integration tests for package
- implement the transaction card mechanism: not sure how to implement.
- update readme in main***
- learn dependency injection 

15.12:
- implement /transactions/packages
- the idea will be, package will have the foreign key from users
- a user has 20 coin and he can buy 4 package that costs 5 each. 
- not sure how to implement
real implement:
- add user_id as a foreign key in the packages table
- I think we should declare a new field inside a user (money)
- still have to fix something inside the repository -> fix to check package is taken or not

5.1:
- update money for users
- implement /card to show all card that user has 
- finish show all cards
- implement the desk method
-> run test for card/ desk

7.1:
- implement tests for desk and card repository

18.1:
- check the code if it works fine with curl scripts
- implement Battle
- fix some errors with curl scripts

19.1:
- implement Battle
- finish implement Battle

22.1:
- write unit test
- implement trading

23.1:
- still need to implement more unit tests
- implement trading
- have to improve the game mechanism: remove from deck ... etc

