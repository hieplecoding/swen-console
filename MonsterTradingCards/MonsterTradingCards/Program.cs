﻿using MonsterTradingCards.Controllers;
using MonsterTradingCards.Database;
using MonsterTradingCards.Http;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Interface.Battles;
using MonsterTradingCards.Interface.ScoreBoard;
using MonsterTradingCards.Interface.Stats;
using MonsterTradingCards.Interface.Trading;
using MonsterTradingCards.Repositories;
using MonsterTradingCards.Services;

namespace MonsterTradingCards;

class Program
{
    private static IUserService _userService;
    private static IPackageService _packageService;
    private static ICardService _cardService;
    private static IDeskService _deckService;
    private static IStatsService _statsService;
    private static IScoreBoardService _scoreBoardService;
    private static IBattlesService _battlesService;
    private static ITradingService _tradingService;


    static void Main()
    {
        // Initialize dependencies

        var dbConnection = new DatabaseConnection();
        //User
        IUserRepository userRepository = new UserRepository(dbConnection);
        _userService = new UserService(userRepository);

        // Package
        IPackageRepository packageRepository = new PackageRepository(dbConnection);
        _packageService = new PackageService(packageRepository);

        // Cards
        ICardRepository cardRepository = new CardRepository(dbConnection);
        _cardService = new CardService(cardRepository);

        // Desk
        IDeskRepository deskRepository = new DeskRepository(dbConnection);
        _deckService = new DeskService(deskRepository, userRepository);

        // Stats
        IStatsRepository statsRepository = new StatsRepository(dbConnection);
        _statsService = new StatsService(statsRepository);

        // Scoreboard
        IScoreBoardRepository scoreBoardRepository = new ScoreBoardRepository(dbConnection);
        _scoreBoardService = new ScoreBoardService(scoreBoardRepository);

        // Battles
        IBattlesRepository battlesRepository = new BattlesRepository(dbConnection);
        _battlesService = new BattlesService(battlesRepository);
        
        // Trading
        ITradingRepository tradingRepository = new TradingRepository(dbConnection);
        _tradingService = new TradingService(tradingRepository);

        // test database connection
        Console.WriteLine(userRepository.TestDatabaseConnection()
            ? "Database connection successful."
            : "Database connection failed.");

        // Initialize controllers
        var userController = new UserController(_userService);
        var packageController = new PackageController(_packageService, _userService);
        var cardController = new CardController(_userService, _cardService);
        var deckController = new DeskController(_userService, _deckService);
        var statsController = new StatsController(_statsService, _userService);
        var scoreBoardController = new ScoreBoardController(_scoreBoardService, _userService);
        var battlesController = new BattlesController(_userService, _battlesService);
        var tradingController = new TradingController(_tradingService, _userService);

        // HTTP part
        HttpSvr svr = new();
        svr.Incoming += (_, e) =>
        {
            if (e.Path == "/users")
            {
                userController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/users/"))
            {
                userController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/users/"))
            {
                userController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/sessions"))
            {
                userController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/packages"))
            {
                packageController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/transactions/"))
            {
                packageController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/cards"))
            {
                cardController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/deck"))
            {
                deckController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/deck"))
            {
                deckController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/stats"))
            {
                statsController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/scoreboard"))
            {
                scoreBoardController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/battles"))
            {
                // enter the lobby to start a battle 
                battlesController.ProcessMessage(e);
            }
            else if (e.Path.StartsWith("/tradings"))
            {
                // enter the lobby to start a battle 
                tradingController.ProcessMessage(e);
            }

            else
            {
                // Handle other routes or return 404 Not Found
                e.Reply(404, "Not Found");
            }
        };

        // Start the HTTP server in a separate thread
        
        Thread serverThread = new Thread(() => svr.Run());
        serverThread.Start();

        Console.WriteLine("Server started. Press Enter to stop.");

        // Wait for the Enter key to stop the server
        Console.ReadLine();

        Console.WriteLine("Stopping server...");
        svr.Stop(); // Stop the server
        serverThread.Join(); // Wait for the server thread to finish
        Console.WriteLine("Server stopped.");

        //svr.Run();

        // Game part
        // /*Game game = new Game();
        // 
        // game.InitializeGame();
        // Console.WriteLine("Game initialized with random cards for each team.");
        // 
        // // Execute rounds until a team runs out of cards or 100 rounds are reached
        // while (!game.IsGameOver())
        // {
        // game.ExecuteRound();
        // }
        // 
        // 
        // // Determine and display the outcome of the game
        // string outcome = game.DetermineOutcome();
        // Console.WriteLine(outcome);*/
        // 
    }
}