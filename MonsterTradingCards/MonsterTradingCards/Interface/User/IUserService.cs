using System.Net.Sockets;
using MonsterTradingCards.Models;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface;

public interface IUserService
{
    bool RegisterUser(UserCredentials user);
    UserCredentials? RetrieveUserData(string username);
    UserData? UpdateUserData(string username, UserData updatedUserData);
    bool LoginUser(UserCredentials user);
    string GenerateToken(string username);
    bool ValidateToken(string token);

    User? GetUserAndCardInDeck(string username);
}