using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface;

public interface IUserRepository
{
    bool TestDatabaseConnection();
    bool UserExists(string username);
    void AddUser(UserCredentials user);
    UserCredentials? GetUserByUsername(string username);
    UserData? UpdateUserDataByUsername(string username, UserData updatedUserData);
    bool LoginUser(string username, string password);
    public int GetUserId(string username);
    User? GetUserAndCardInDeck(string username);

    List<Guid> GetCardInDeskByUsernameRepo(string username);

    UserStats? GetUserStatsByUsername(string username);

    Card? GetCardById(Guid cardid);
}