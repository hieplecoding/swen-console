using System.Net.Sockets;
using System.Threading.Tasks;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface.Battles
{
    public interface IBattlesService
    {
        Task<string> EnterLobbyAsync(User user, TcpClient client);
        void WaitsForGame();
        bool IsGameOver();
        Task<string> ExecuteRoundAsync(string username1, string username2);
        string HandleBattleOutcome(Card team1Card, Card team2Card, int team1Result, int team2Result, string username1, string username2);
        string DetermineOutcome(string username1, string username2);

        public void RemoveLoseCardFromDeckService(Card card);

        int CountCardInDeckService(string username);
    }
}