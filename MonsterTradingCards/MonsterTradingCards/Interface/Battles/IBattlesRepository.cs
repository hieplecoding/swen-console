using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface.Battles;

public interface IBattlesRepository
{
    public void ChangeStatsAfterBattle(string userWin,string userLose);
    public void RemoveLoseCardFromDeckRepo(Card card);
    
    int CountCardInDeckRepo(string username);
}