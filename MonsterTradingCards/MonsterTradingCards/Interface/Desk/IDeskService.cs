using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface;

public interface IDeskService
{
    public List<Card> GetCardInDeskByUsernameService(string username);
    bool PutCardInDesk(string username, List<Guid> cardIds);

    bool CheckIfUserHasCard(string username, List<Guid> cardIds);
}