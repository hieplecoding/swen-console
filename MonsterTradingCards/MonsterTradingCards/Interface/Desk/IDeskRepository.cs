using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface;

public interface IDeskRepository
{
    public bool AddsUserDeck(int userId, List<Guid> cardIds);

    public bool CheckIfUserHasCardInDeskRepo(string username, List<Guid> cardIds);
    public List<Card> GetCardInDeskByUsernameRepo(string username);
}