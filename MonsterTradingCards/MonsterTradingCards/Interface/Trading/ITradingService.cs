using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface.Trading;

public interface ITradingService
{
    public List<TradingDeal> GetTradingDealService(string username);

    bool CheckCardIfUserNotOwnOrInDeckService(TradingDeal? tradingDeal, string username);

    bool CheckDealIsAlreadyExistService(TradingDeal? tradingDeal, string username);

    public void CreateTradingService(TradingDeal? tradingDeal);

    bool UserHasDealService(Guid tradingId, string username);

    bool FindDealService(Guid tradingId);

    public void DeleteTradingService(Guid tradingId);

    bool IfCardIsGoodEnoughServiceOrNotYourOwnCardService(Guid tradingId, string username);

    public void DoTradingService(Guid tradingIdHas, Guid tradingIdWantToTrade, Guid idToDelete);

    public Guid FindTheCardOfThatTradingIdService(Guid tradingId);

    public bool UserTryToTradeToHimSelfService(Guid tradingId, string username);
    
    // new
    public bool CheckCardOwnership(string username, Guid cardId);
    public bool IsCardInDeck(string username, Guid cardId);
}