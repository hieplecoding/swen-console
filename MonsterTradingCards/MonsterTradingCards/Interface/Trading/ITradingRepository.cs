using MonsterTradingCards.Models.ModelsForGame;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface.Trading;

public interface ITradingRepository
{
    public List<TradingDeal> GetTradingDealRepository(string username);
    bool CheckCardIfUserNotOwnOrInDeckRepo(TradingDeal? tradingDeal, string username);
    bool CheckDealIsAlreadyExistRepo(TradingDeal? tradingDeal, string username);
    
    public void CreateTradingRepo(TradingDeal? tradingDeal);
    
    bool UserHasDealRepository(Guid tradingId, string username);
    
    bool FindDealRepo(Guid tradingId);
    
    public void DeleteTradingRepo(Guid tradingId);
    
    bool IfCardIsGoodEnoughServiceOrNotYourOwnCardRepo(Guid tradingId, string username);
    
    public void DoTradingRepo(Guid tradingIdHas, Guid tradingIdWantToTrade, Guid idToDelete);
    
    public Guid FindTheCardOfThatTradingIdRepo(Guid tradingId);
    
    public bool UserTryToTradeToHimSelfRepo(Guid tradingId, string username);
    public void UpdateCardPackage(Guid cardId, Guid newPackageId);
    public int FindUserIdWithGuidCard(Guid tradingId);

    public List<Card> ListOfCardInPackageFromUser(int userId);
    public Guid GetPackageIdByCardId(Guid cardId);

    public int GetUserIdByUsername(string username);
    
    // new
    public bool CheckCardOwnership(string username, Guid cardId);
    public bool IsCardInDeck(string username, Guid cardId);

}