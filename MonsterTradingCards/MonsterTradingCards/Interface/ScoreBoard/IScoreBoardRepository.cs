using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface.ScoreBoard;

public interface IScoreBoardRepository
{
    List<UserStats> GetUserStatsByUserElo();
}