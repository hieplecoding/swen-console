using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface.ScoreBoard;

public interface IScoreBoardService
{
    List<UserStats> GetUserStats();
}