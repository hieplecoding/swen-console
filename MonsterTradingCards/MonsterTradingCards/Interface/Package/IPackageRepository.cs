using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface;

public interface IPackageRepository
{
    bool AddCards(List<Card> cards);
    bool AddPackageToUser(string username);
    bool CheckPackageToBuy();

    bool checkMoney(string username);
}