using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface;

public interface IPackageService
{
    bool CreatePackage(List<Card> cards);
    bool BuyPackage(string username);
    bool CheckPackage();

    bool checkMoney(string username);

}