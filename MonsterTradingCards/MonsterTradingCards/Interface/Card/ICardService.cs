using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface;
using MonsterTradingCards.Models;

public interface ICardService
{
    public List<Card> GetAllCardsForUser(string username);
}