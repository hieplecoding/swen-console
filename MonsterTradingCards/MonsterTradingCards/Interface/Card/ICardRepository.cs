using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Interface;
using MonsterTradingCards.Models;
public interface ICardRepository
{

    public List<Card> GetCardsByUsername(string username);
}