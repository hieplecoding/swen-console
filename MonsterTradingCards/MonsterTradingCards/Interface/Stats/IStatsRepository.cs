using MonsterTradingCards.Models;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface.Stats;

public interface IStatsRepository
{
    UserStats? GetUserStatsByUsername(string username);
}