using MonsterTradingCards.Models;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Interface.Stats;

public interface IStatsService
{
  UserStats? GetUserStats(string userElo);
}