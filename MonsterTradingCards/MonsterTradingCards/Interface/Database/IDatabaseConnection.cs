using Npgsql;

namespace MonsterTradingCards.Interface.Database;

public interface IDatabaseConnection
{
    public NpgsqlConnection GetConnection();
}