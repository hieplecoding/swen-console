using System.IdentityModel.Tokens.Jwt;
using MonsterTradingCards.Http;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;
using MonsterTradingCards.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MonsterTradingCards.Controllers;

public class PackageController
{
    private readonly IPackageService _packageService;
    private readonly IUserService _userService; // Access the token validation method

    public PackageController(IPackageService packageService, IUserService userService)
    {
        _packageService = packageService;
        _userService = userService;
    }

    public void ProcessMessage(HttpSvrEventArgs e)
    {
        if (e.Method == "POST" && e.Path == "/packages")
        {
            HandlePackage(e);
        }
        else if (e.Method == "POST" && e.Path == "/transactions/packages")
        {
            HandleTransactionPackage(e);
        }
    }

    /// <summary>
    /// buy package for user with money 20 coin
    /// </summary>
    /// <param name="e"></param>
    /// <param name="moneyForUser"></param>
    /// <exception cref="NotImplementedException"></exception>
    private void HandleTransactionPackage(HttpSvrEventArgs e)
    {
        // package position
        /*
        var jsonObject = JObject.Parse(e.Payload);

        var positionValue = jsonObject["position"]?.ToString();
        int packagePosition = Convert.ToInt32(positionValue);
        */


        string token = null;

        // Iterate through the headers to find the Authorization header
        foreach (var header in e.Headers)
        {
            if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
            {
                token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                break;
            }
        }

        if (string.IsNullOrEmpty(token))
        {
            e.Reply(401, "Access token is missing or invalid");
            return;
        }

        if (!_userService.ValidateToken(token))
        {
            e.Reply(401, "Access token is missing or invalid");
            return;
        }

        // can not buy if there is no package
        if (!_packageService.CheckPackage())
        {
            e.Reply(404, "No card package available for buying");
            return;
        }


        // Decode the token to get the username
        var username = TokenUtilities.DecodeTokenUsername(token);

        bool checkIfUserHasMoney = _packageService.checkMoney(username);
        if (!checkIfUserHasMoney)
        {
            e.Reply(403, "Not enough money for buying a card package");
            return;
        }

        bool result = _packageService.BuyPackage(username);
        if (!result)
        {
            e.Reply(404, "Your package is already taken or your package is not wrong");
            return;
        }

        e.Reply(200, "A package has been successfully bought");
    }

    /// <summary>
    /// only admin can add package into database
    /// </summary>
    /// <param name="e"></param>
    private void HandlePackage(HttpSvrEventArgs e)
    {
        string token = null;

        // Iterate through the headers to find the Authorization header
        foreach (var header in e.Headers)
        {
            if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
            {
                token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                break;
            }
        }

        if (string.IsNullOrEmpty(token))
        {
            e.Reply(401, "Access token is missing or invalid");
            return;
        }

        if (!_userService.ValidateToken(token))
        {
            e.Reply(401, "Access token is missing or invalid");
            return;
        }

        // Decode the token to get the username
        var username = TokenUtilities.DecodeTokenUsername(token);
        if (username != "admin")
        {
            e.Reply(403, "Provided user is not 'admin'");
            return;
        }

        // Simplified response for testing admin access
        //e.Reply(200, "Admin user access confirmed.");

        // Commented out package creation logic for testing purposes

        try
        {
            var cards = JsonConvert.DeserializeObject<List<Card>>(e.Payload);
            if (cards == null)
            {
                e.Reply(400, "Invalid request payload");
                return;
            }


            if (_packageService.CreatePackage(cards))
            {
                e.Reply(201, "Package and cards successfully created");
            }
            else
            {
                e.Reply(409, "At least one card in the packages already exists");
            }
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }
}