using MonsterTradingCards.Http;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Interface.Battles;
using MonsterTradingCards.Utilities;

namespace MonsterTradingCards.Controllers;

public class BattlesController
{
    private readonly IUserService _userService; // Access the token validation method
    private readonly IBattlesService _battlesService;
    
    public BattlesController(IUserService userService, IBattlesService battlesService)
    {
        _userService = userService;
        _battlesService = battlesService;
    }
    
    public void ProcessMessage(HttpSvrEventArgs e)
    {
        try
        {
            if (e.Method == "POST")
            {
                HandleBattles(e);
            }
            
        }
        catch (Exception exception)
        {
            e.Reply(404, "Not Found");
        }
        
    }

    private async Task HandleBattles(HttpSvrEventArgs e)
    {
        try
        {


            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            // implement game logic
            // Retrieve the user
            
            var user = _userService.GetUserAndCardInDeck(username);

            if (user == null)
            {
                e.Reply(500, "Error to get user stats and card in deck");
            }

            // Start the game in a new thread and pass the client
            if (user != null)
            {
                string gameResult = await _battlesService.EnterLobbyAsync(user, e.Client);
                e.Reply(200, gameResult); // Reply with the game result
            }
            else
            {
                e.Reply(500, "Error to get user stats and card in deck");
            }
            
            
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }
}