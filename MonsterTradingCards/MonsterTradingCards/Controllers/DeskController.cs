using MonsterTradingCards.Http;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Models.ModelsForGame;
using MonsterTradingCards.Utilities;
using Newtonsoft.Json;

namespace MonsterTradingCards.Controllers;

public class DeskController
{
    private readonly IUserService _userService; // Access the token validation method
    private readonly IDeskService _deskService;

    public DeskController(IUserService userService, IDeskService deskService)
    {
        _userService = userService;
        _deskService = deskService;
    }

    public void ProcessMessage(HttpSvrEventArgs e)
    {
        try
        {
            if (e.Method == "GET")
            {
                var pathAndQuery = e.Path.Split('?');
                var path = pathAndQuery[0];
                var queryString = pathAndQuery.Length > 1 ? pathAndQuery[1] : "";

                if (path == "/deck")
                {
                    if (queryString.Contains("format=plain"))
                    {
                        HandleGetDeskPlainText(e);
                    }
                    else
                    {
                        HandleGetDesk(e);
                    }
                }
            }

            if (e.Method == "PUT")
            {
                HandlePutDesk(e);
            }
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }

    private void HandleGetDeskPlainText(HttpSvrEventArgs e)
    {
        try
        {
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);
            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var getCardInDesk = _deskService.GetCardInDeskByUsernameService(username);

            if (getCardInDesk == null || !getCardInDesk.Any())
            {
                e.Reply(204);
                return;
            }

            // Convert the deck to plain text
            var plainTextResponse = ConvertDeckToPlainText(getCardInDesk);
            e.Reply(200, plainTextResponse);
        }
        catch (Exception)
        {
            e.Reply(500, "Internal server error");
        }
    }

    private string ConvertDeckToPlainText(IEnumerable<Card> cards)
    {
        // Format the card data as plain text
        return string.Join(Environment.NewLine, cards.Select(card => 
            $"Id: {card.Id}, Name: {card.Name}, Damage: {card.Damage}"));
    }


    private void HandlePutDesk(HttpSvrEventArgs e)
    {
        try
        {
            // package position
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);
            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            // implement
            var cardIds = JsonConvert.DeserializeObject<List<Guid>>(e.Payload);
            if (cardIds == null || cardIds.Count != 4)
            {
                e.Reply(400, "The provided deck did not include the required amount of cards");
                return;
            }

            if (!_deskService.CheckIfUserHasCard(username, cardIds))
            {
                e.Reply(403, "At least one of the provided cards does not belong to the user or is not available.");
                return;
            }

            var cardsInDesk = _deskService.PutCardInDesk(username, cardIds);
            e.Reply(200, "The deck has been successfully configured");
        }
        catch (Exception)
        {
            e.Reply(500, "Internal server error");
        }
    }

    private void HandleGetDesk(HttpSvrEventArgs e)
    {
        try
        {
            // package position
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);
            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            // implement 
            var getCardInDesk = _deskService.GetCardInDeskByUsernameService(username);
            var json = JsonConvert.SerializeObject(getCardInDesk);

            if (String.IsNullOrEmpty(json) || json == "[]")
            {
                e.Reply(204);
            }
            else
            {
                e.Reply(200, json);
            }
        }
        catch (Exception)
        {
            e.Reply(500, "Internal server error");
        }
    }
}