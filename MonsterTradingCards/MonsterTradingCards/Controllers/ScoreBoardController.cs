using MonsterTradingCards.Interface;
using MonsterTradingCards.Interface.ScoreBoard;
using MonsterTradingCards.Utilities;


namespace MonsterTradingCards.Controllers;

using Newtonsoft.Json;
using Http;

public class ScoreBoardController
{
    private readonly IScoreBoardService _scoreBoardService;
    private readonly IUserService _userService;

    public ScoreBoardController(IScoreBoardService statsScoreBoardService, IUserService userService)
    {
        _scoreBoardService = statsScoreBoardService;
        _userService = userService;
    }

    public void ProcessMessage(HttpSvrEventArgs e)
    {
        if (e.Method == "GET")
        {
            HandleGetStats(e);
        }

        else
        {
            // Handle other routes or return 404 Not Found
            e.Reply(404, "Not Found");
        }
    }

    private void HandleGetStats(HttpSvrEventArgs e)
    {
        try
        {
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            // Convert userStats object to JSON

            var response = JsonConvert.SerializeObject(_scoreBoardService.GetUserStats());
            e.Reply(200, response);

            
        }
        catch (JsonSerializationException ex)
        {
            // This block will execute if the payload cannot be deserialized to an integer.
            Console.WriteLine($"Error deserializing payload to integer: {ex.Message}");
            e.Reply(400, "Invalid input: userElo must be an integer."); // Reply with a 400 Bad Request
        }
        catch (Exception ex)
        {
            // This block will catch other general exceptions.
            Console.WriteLine($"An error occurred: {ex.Message}");
            e.Reply(500, "Internal server error."); // Reply with a 500 Internal Server Error
        }
    }
}