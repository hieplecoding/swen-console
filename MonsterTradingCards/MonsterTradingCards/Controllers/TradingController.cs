using MonsterTradingCards.Http;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Interface.Trading;
using MonsterTradingCards.Utilities;
using MonterTradingCards.Models;
using Newtonsoft.Json;

namespace MonsterTradingCards.Controllers;

public class TradingController
{
    private readonly ITradingService _tradingService;
    private readonly IUserService _userService;

    public TradingController(ITradingService tradingService, IUserService userService)
    {
        _tradingService = tradingService;
        _userService = userService;
    }

    public void ProcessMessage(HttpSvrEventArgs e)
    {
        if (e.Method == "GET")
        {
            HandleGetTrading(e);
        }

        if (e.Method == "POST" && e.Path == "/tradings")
        {
            HandleCreateTrading(e);
        }

        if (e.Method == "POST" && e.Path.StartsWith("/tradings/"))
        {
            HandleTrading(e);
        }

        if (e.Method == "DELETE")
        {
            HandleDeleteTrading(e);
        }

        else
        {
            // Handle other routes or return 404 Not Found
            e.Reply(404, "Not Found");
        }
    }

    private void HandleTrading(HttpSvrEventArgs e)
    {
        try
        {
            // Extract the tradingdealid from the URL
            string[] pathSegments = e.Path.Split('/');
            if (pathSegments.Length < 3 || !Guid.TryParse(pathSegments[2], out Guid tradingDealId))
            {
                e.Reply(400, "Invalid trading deal ID"); // Bad Request
                return;
            }

            // the card want to be traded
            Guid tradingIdWantTrade = new Guid(pathSegments[2]);
            Guid idOfTheCardWantToTrade =  _tradingService.FindTheCardOfThatTradingIdService(tradingIdWantTrade);
            // the card the user offers
            var tradingCardIdHas = JsonConvert.DeserializeObject<Guid>(e.Payload);
            
            //Guid tradingCardIdHas = tradingInfo.Id;


            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }


            // 404
            // The provided deal ID was not found.
            if (!_tradingService.FindDealService(tradingIdWantTrade))
            {
                e.Reply(404, "The provided deal ID was not found");
                return;
            }

            // 403
            // The offered card is not owned by the user,
            // or the requirements are not met (Type, MinimumDamage), or the offered card is locked in the deck.
            if (!_tradingService.IfCardIsGoodEnoughServiceOrNotYourOwnCardService(tradingIdWantTrade, username))
            {
                e.Reply(403,
                    "The offered card is not owned by the user, or the requirements are not met (Type, MinimumDamage), or the offered card is locked in the deck.");
                return;
            }
            // prevent not to trade with your self
            if (_tradingService.UserTryToTradeToHimSelfService(tradingIdWantTrade, username))
            {
                e.Reply(403,
                    "The offered card is not owned by the user, or the requirements are not met (Type, MinimumDamage), or the offered card is locked in the deck.");
                return;
            }
            // card id
            _tradingService.DoTradingService(tradingCardIdHas, idOfTheCardWantToTrade, tradingIdWantTrade);


            e.Reply(200, "Trading deal successfully executed"); // OK
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message); // Internal Server Error
        }
    }

    private void HandleDeleteTrading(HttpSvrEventArgs e)
    {
        try
        {
            // Extract the tradingdealid from the URL
            string[] pathSegments = e.Path.Split('/');
            if (pathSegments.Length < 3 || !Guid.TryParse(pathSegments[2], out Guid tradingDealId))
            {
                e.Reply(400, "Invalid trading deal ID"); // Bad Request
                return;
            }

            Guid tradingId = new Guid(pathSegments[2]);

            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            // 403
            // The deal contains a card that is not owned by the user.
            if (!_tradingService.UserHasDealService(tradingId, username))
            {
                e.Reply(403, "The deal contains a card that is not owned by the user.");
                return;
            }

            // 404
            // The provided deal ID was not found.
            if (!_tradingService.FindDealService(tradingId))
            {
                e.Reply(404, "The provided deal ID was not found");
                return;
            }
            // 409 
            // A deal with this deal ID already exists.


            _tradingService.DeleteTradingService(tradingId);
            e.Reply(200, "Trading deal deleted successfully"); // OK
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message); // Internal Server Error
        }
    }

    private void HandleGetTrading(HttpSvrEventArgs e)
    {
        try
        {
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var getTradingDeal = _tradingService.GetTradingDealService(username);
            var json = JsonConvert.SerializeObject(getTradingDeal);

            if (String.IsNullOrEmpty(json) || json == "[]")
            {
                e.Reply(204);
            }
            else
            {
                e.Reply(200, json);
            }
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }


    private void HandleCreateTrading(HttpSvrEventArgs e)
    {
        try
        {
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            TradingDeal? tradingDeal = JsonConvert.DeserializeObject<TradingDeal>(e.Payload);

            if (tradingDeal == null)
            {
                e.Reply(400, "no trading information");
                return;
            }

            // return 403 
            // The deal contains a card that is not owned by the user or locked in the deck.
            /*
            if (!_tradingService.CheckCardIfUserNotOwnOrInDeckService(tradingDeal, username))
            {
                e.Reply(403, "The deal contains a card that is not owned by the user or locked in the deck.");
                return;
            }
            */
            if (!_tradingService.CheckCardOwnership(username, tradingDeal.CardToTrade))
            {
                e.Reply(403, "The deal contains a card that is not owned by the user or locked in the deck.");
                return;
            }

            if (_tradingService.IsCardInDeck(username, tradingDeal.CardToTrade))
            {
                e.Reply(403, "The deal contains a card that is not owned by the user or locked in the deck.");
                return;
            }
            // return 409
            // A deal with this deal ID already exists
            if (_tradingService.CheckDealIsAlreadyExistService(tradingDeal, username))
            {
                e.Reply(409, "A deal with this deal ID already exists");
                return;
            }

            // return 201
            // Trading deal successfully created
            _tradingService.CreateTradingService(tradingDeal);
            e.Reply(201, "Trading deal successfully created");
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }
}