using System.IdentityModel.Tokens.Jwt;

using MonsterTradingCards.Http;
using MonsterTradingCards.Interface;

using Newtonsoft.Json;


namespace MonsterTradingCards.Controllers;

using MonsterTradingCards.Utilities;

public class CardController
{
    private readonly IUserService _userService; // Access the token validation method
    private readonly ICardService _cardService;

    public CardController(IUserService userService, ICardService cardService)
    {
        _userService = userService;
        _cardService = cardService;
    }

    public void ProcessMessage(HttpSvrEventArgs e)
    {
        try
        {
            if (e.Method == "GET")
            {
                HandleSearchCard(e);
            }
            
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
        
    }

    private void HandleSearchCard(HttpSvrEventArgs e)
    {
        try
        {

            // package position
            
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }
            
            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var cards = _cardService.GetAllCardsForUser(username);
            if (cards.Count == 0)
            {
                // No cards found in the user's deck
                e.Reply(204, "The request was fine, but the user doesn't have any cards"); // 204 No Content
                return;
            }

            var json = JsonConvert.SerializeObject(cards);
            e.Reply(200, json);
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }


}