using MonsterTradingCards.Interface;
using MonsterTradingCards.Utilities;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Controllers;

using Newtonsoft.Json;
using Http;
using Models;


public class UserController
{
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
        
    }

    /// <summary>
    /// handle the http request for users
    /// </summary>
    /// <param name="e"></param>
    public void ProcessMessage(HttpSvrEventArgs e)
    {
        if (e.Method == "POST" && e.Path == "/users")
        {
            HandleUserRegistration(e);
        }
        else if (e.Method == "GET" && e.Path.StartsWith("/users/"))
        {
            HandleGetUserData(e);
        }
        else if (e.Method == "PUT" && e.Path.StartsWith("/users/"))
        {
            HandleUpdateUserData(e);
        }
        else if (e.Method == "POST" && e.Path.StartsWith("/sessions"))
        {
            HandleUserLogin(e);
        }
        /*
        else if (e.Method == "GET" && e.Path.StartsWith("/battles"))
        {
            // test for battle
            HandleGetUserStatAndCard(e);
        */
        else
        {
            // Handle other routes or return 404 Not Found
            e.Reply(404, "Not Found");
        }
    }

    private void HandleGetUserStatAndCard(HttpSvrEventArgs e)
    {
        try
        {
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }
            // Convert userStats object to JSON
            var response = JsonConvert.SerializeObject(_userService.GetUserAndCardInDeck(username));
            e.Reply(200, response);
            
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }

    /// <summary>
    /// allow to register if that username is not already in database
    /// </summary>
    /// <param name="e"></param>
    private void HandleUserRegistration(HttpSvrEventArgs e)
    {
        try
        {
            var userCredentials = JsonConvert.DeserializeObject<UserCredentials>(e.Payload);
            if (userCredentials == null || string.IsNullOrWhiteSpace(userCredentials.Username) || string.IsNullOrWhiteSpace(userCredentials.Password))
            {
                e.Reply(400, "Invalid request");
                return;
            }

            bool userCreated = _userService.RegisterUser(userCredentials);
            if (userCreated)
            {
                e.Reply(201, "User successfully created");
            }
            else
            {
                e.Reply(409, "User with the same username already registered");
            }
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }

    /// <summary>
    /// search username
    /// </summary>
    /// <param name="e"></param>
    private void HandleGetUserData(HttpSvrEventArgs e)
    {
        try
        {
            // Extract the username from the path
            string usernameFromUrl = e.Path.Substring("/users/".Length);
            
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (usernameFromUrl != username)
            {
                e.Reply(404, "User not found.");
                return;
            }
            // Retrieve user data
            var userData = _userService.RetrieveUserData(usernameFromUrl);
            if (userData != null)
            {
                
                e.Reply(200, $"{usernameFromUrl} is found!");
            }
            else
            {
                e.Reply(404, "User not found");
            }
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }

    /// <summary>
    /// update default user data
    /// </summary>
    /// <param name="e"></param>
    private void HandleUpdateUserData(HttpSvrEventArgs e)
    {
        try
        {
            // Extract the username from the path
            string usernameFromUrl = e.Path.Substring("/users/".Length);
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (usernameFromUrl != username)
            {
                e.Reply(404, "User not found.");
                return;
            }

            // Deserialize the JSON request body to get the updated user data
            var requestBody = JsonConvert.DeserializeObject<UserData>(e.Payload);

            if (requestBody == null)
            {
                return;
            }
            // Update user data
            var updatedUserData = _userService.UpdateUserData(usernameFromUrl, requestBody);

            if (updatedUserData != null)
            {
                //string jsonResponse = JsonConvert.SerializeObject(updatedUserData);
                e.Reply(200, $"{usernameFromUrl} updated successfully!");
            }
            else
            {
                e.Reply(404, "User not found");
            }
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }
    
    // sessions
    /// <summary>
    /// Login with correct username and password
    /// </summary>
    /// <param name="e"></param>
    private void HandleUserLogin(HttpSvrEventArgs e)
    {
        try
        {
            var userCredentials = JsonConvert.DeserializeObject<UserCredentials>(e.Payload);
            if (userCredentials == null || string.IsNullOrWhiteSpace(userCredentials.Username) || string.IsNullOrWhiteSpace(userCredentials.Password))
            {
                e.Reply(400, "Invalid request");
                return;
            }

            bool userLogin = _userService.LoginUser(userCredentials);
            if (userLogin)
            {
                var token = _userService.GenerateToken(userCredentials.Username);
                e.Reply(201, "User login successful " + JsonConvert.SerializeObject(new { token }));
            }
            else
            {
                e.Reply(401, "Invalid username/password provided");
            }
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }
}
