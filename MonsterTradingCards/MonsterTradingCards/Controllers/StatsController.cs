using MonsterTradingCards.Interface;
using MonsterTradingCards.Interface.Stats;
using MonsterTradingCards.Utilities;


namespace MonsterTradingCards.Controllers;

using Newtonsoft.Json;
using Http;


public class StatsController
{
    private readonly IStatsService _statsService;
    private readonly IUserService _userService;

    public StatsController(IStatsService statsService, IUserService userService)
    {
        _statsService = statsService;
        _userService = userService;
    }

    public void ProcessMessage(HttpSvrEventArgs e)
    {
        if (e.Method == "GET")
        {
            HandleGetStats(e);
        }

        else
        {
            // Handle other routes or return 404 Not Found
            e.Reply(404, "Not Found");
        }
    }

    private void HandleGetStats(HttpSvrEventArgs e)
    {
        try
        {
            string token = null;

            // Iterate through the headers to find the Authorization header
            foreach (var header in e.Headers)
            {
                if (header.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Value.StartsWith("Bearer ") ? header.Value.Substring("Bearer ".Length) : null;
                    break;
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!_userService.ValidateToken(token))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = TokenUtilities.DecodeTokenUsername(token);

            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }
            // Convert userStats object to JSON
            var response = JsonConvert.SerializeObject(_statsService.GetUserStats(username));
            e.Reply(200, response);
            
        }
        catch (Exception ex)
        {
            e.Reply(500, "Internal Server Error: " + ex.Message);
        }
    }
}