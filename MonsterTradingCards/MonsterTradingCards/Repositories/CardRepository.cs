using MonsterTradingCards.Database;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;
using Npgsql;

namespace MonsterTradingCards.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public CardRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public List<Card> GetCardsByUsername(string username)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                var cards = new List<Card>();

                using (var command = new NpgsqlCommand(@"
                    SELECT c.cardid, c.cardtype, c.damage 
                    FROM cards c
                    INNER JOIN packages p ON c.packageid = p.packageid
                    INNER JOIN users u ON p.user_id = u.id
                    WHERE u.username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new Card(
                                reader.GetGuid(0),   // Id
                                reader.GetString(1), // Name
                                reader.GetDouble(2)  // Damage
                            );
                            cards.Add(card);
                        }
                    }
                }

                return cards;
            }
        }
    }
}