using System.Data;
using MonsterTradingCards.Interface.Database;
using MonsterTradingCards.Interface.Trading;
using MonsterTradingCards.Models.ModelsForGame;
using MonterTradingCards.Models;
using Npgsql;

namespace MonsterTradingCards.Repositories;

public class TradingRepository : ITradingRepository
{
    private readonly IDatabaseConnection _databaseConnection;

    public TradingRepository(IDatabaseConnection databaseConnection)
    {
        _databaseConnection = databaseConnection;
    }

    public List<TradingDeal> GetTradingDealRepository(string username)
    {
        var tradingDeals = new List<TradingDeal>();

        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                using (var command = new NpgsqlCommand(@"
            SELECT t.id, t.cardtotrade, t.type, t.minimum_damage
            FROM trading t
            INNER JOIN cards c ON t.cardtotrade = c.cardid
            INNER JOIN packages p ON c.packageid = p.packageid
            INNER JOIN users u ON p.user_id = u.id
            WHERE u.username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var tradingDeal = new TradingDeal
                            {
                                Id = reader.GetGuid(reader.GetOrdinal("id")),
                                CardToTrade = reader.GetGuid(reader.GetOrdinal("cardtotrade")),
                                Type = reader["type"].ToString(),
                                MinimumDamage = Convert.ToDouble(reader["minimum_damage"])
                            };
                            tradingDeals.Add(tradingDeal);
                        }
                    }
                }
            }
        }
        catch (NpgsqlException ex)
        {
            Console.WriteLine($"Error in GetTradingDealRepository: {ex.Message}");
            // Consider how to handle the exception, possibly rethrow or return an empty list
        }

        return tradingDeals;
    }


    public bool CheckCardIfUserNotOwnOrInDeckRepo(TradingDeal? tradingDeal, string username)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT 
                    (CASE 
                        WHEN c.cardid IS NULL THEN 'false'  -- The user does not own the card
                        WHEN ud.cardid IS NOT NULL THEN 'false' -- The card is in the user's deck
                        ELSE 'true'  -- The user owns the card and it's not in the deck
                    END) AS result
                FROM users u
                LEFT JOIN packages p ON u.id = p.user_id
                LEFT JOIN cards c ON p.packageid = c.packageid AND c.cardid = @CardToTrade
                LEFT JOIN user_decks ud ON u.id = ud.userid AND c.cardid = ud.cardid
                WHERE u.username = @Username;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@CardToTrade", tradingDeal.CardToTrade);

                    var result = command.ExecuteScalar()?.ToString();
                    return result == "true";
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in CheckCardIfUserNotOwnOrInDeckRepo: {ex.Message}");
            return false;
        }
    }

    public bool CheckDealIsAlreadyExistRepo(TradingDeal? tradingDeal, string username)
    {
        if (tradingDeal == null)
        {
            return false; // No trading deal provided
        }

        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT COUNT(*)
                FROM trading t
                JOIN cards c ON t.cardtotrade = c.cardid
                JOIN packages p ON c.packageid = p.packageid
                JOIN users u ON p.user_id = u.id
                WHERE u.username = @Username AND c.cardid = @CardToTrade;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@CardToTrade", tradingDeal.CardToTrade);

                    var result = (long)command.ExecuteScalar();
                    return result > 0; // True if there's at least one deal for this card by the user
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in CheckDealIsAlreadyExistRepo: {ex.Message}");
            return false;
        }
    }

    public void CreateTradingRepo(TradingDeal? tradingDeal)
    {
        if (tradingDeal == null)
        {
            throw new ArgumentException("Trading deal cannot be null.");
        }

        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                INSERT INTO trading (id, cardtotrade, type, minimum_damage)
                VALUES (@Id, @CardToTrade, @Type, @MinimumDamage);";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Id", tradingDeal.Id);
                    command.Parameters.AddWithValue("@CardToTrade", tradingDeal.CardToTrade);
                    command.Parameters.AddWithValue("@Type", tradingDeal.Type ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@MinimumDamage", tradingDeal.MinimumDamage);

                    command.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in CreateTradingService: {ex.Message}");
            throw; // Consider rethrowing the exception or handling it according to your error policy
        }
    }

    public bool UserHasDealRepository(Guid tradingId, string username)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT COUNT(*)
                FROM trading t
                JOIN cards c ON t.cardtotrade = c.cardid
                JOIN packages p ON c.packageid = p.packageid
                JOIN users u ON p.user_id = u.id
                WHERE u.username = @Username AND t.id = @TradingId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@TradingId", tradingId);

                    var result = (long)command.ExecuteScalar();
                    return result > 0; // True if there's at least one deal with this trading ID by the user
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in UserHasDealRepository: {ex.Message}");
            return false;
        }
    }

    public bool FindDealRepo(Guid tradingId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT COUNT(*)
                FROM trading
                WHERE id = @TradingId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@TradingId", tradingId);

                    var result = (long)command.ExecuteScalar();
                    return result > 0; // True if the trading deal exists
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in FindDealRepo: {ex.Message}");
            return false;
        }
    }

    public void DeleteTradingRepo(Guid tradingId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = "DELETE FROM trading WHERE id = @TradingId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@TradingId", tradingId);
                    command.ExecuteNonQuery(); // Execute the DELETE command
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in DeleteTradingRepo: {ex.Message}");
        }
    }

    public bool IfCardIsGoodEnoughServiceOrNotYourOwnCardRepo(Guid tradingId, string username)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
            SELECT COUNT(*)
            FROM trading t
            JOIN cards c ON t.cardtotrade = c.cardid
            JOIN packages p ON c.packageid = p.packageid
            JOIN users u ON p.user_id = u.id
            WHERE t.id = @TradingId 
              AND (u.username != @Username OR (t.type = 'monster' AND t.minimum_damage > 10));";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@TradingId", tradingId);
                    command.Parameters.AddWithValue("@Username", username);

                    var result = (long)command.ExecuteScalar();
                    return result > 0; // True if the card meets any of the criteria
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in IfCardIsGoodEnoughServiceAndNotYourOwnCardRepo: {ex.Message}");
            return false;
        }
    }

    public void DoTradingRepo(Guid tradingIdHas, Guid tradingIdWantToTrade, Guid idToDelete)
    {
        using (var connection = _databaseConnection.GetConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    // Verify the ownership of the cards
                    /*
                    int userIdHas = FindUserIdWithGuidCard(tradingIdHas);
                    int userIdWants = FindUserIdWithGuidCard(tradingIdWantToTrade);
                    int userIdInitiating = GetUserIdByUsername(username);
                    */
                    

                    // Get the package IDs for each card
                    Guid packageIdHas = GetPackageIdByCardId(tradingIdHas);
                    Guid packageIdWants = GetPackageIdByCardId(tradingIdWantToTrade);

                    // Update the package IDs of the cards
                    UpdateCardPackage(tradingIdHas, packageIdWants);
                    UpdateCardPackage(tradingIdWantToTrade, packageIdHas);
                    
                    DeleteTradingRepo(idToDelete);

                    // Commit the transaction
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    // Rollback the transaction in case of an error
                    transaction.Rollback();

                    // Log the exception
                    Console.WriteLine($"Error in DoTradingRepo: {ex.Message}");
                }
            }
        }
    }

    public int GetUserIdByUsername(string username)
    {
        using (var connection = _databaseConnection.GetConnection())
        {
            connection.Open();
            using (var command = new NpgsqlCommand("SELECT id FROM users WHERE username = @Username", connection))
            {
                command.Parameters.AddWithValue("@Username", username);

                var result = command.ExecuteScalar();
                if (result != null)
                {
                    return (int)result; // Cast the result to integer and return
                }
                else
                {
                    throw new InvalidOperationException("User not found.");
                }
            }
        }
    }

    public bool CheckCardOwnership(string username, Guid cardId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT COUNT(*)
                FROM cards c
                JOIN packages p ON c.packageid = p.packageid
                JOIN users u ON p.user_id = u.id
                WHERE u.username = @Username AND c.cardid = @CardId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@CardId", cardId);

                    var result = (long)command.ExecuteScalar();
                    return result > 0;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error in CheckCardOwnership: {ex.Message}");
            return false;
        }
    }


    public bool IsCardInDeck(string username, Guid cardId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT COUNT(*)
                FROM user_decks ud
                JOIN users u ON ud.userid = u.id
                WHERE u.username = @Username AND ud.cardid = @CardId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@CardId", cardId);

                    var result = (long)command.ExecuteScalar();
                    return result > 0;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error in IsCardInDeck: {ex.Message}");
            return false;
        }
    }


    public bool UserTryToTradeToHimSelfRepo(Guid tradingId, string username)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT COUNT(*)
                FROM trading t
                JOIN cards c ON t.cardtotrade = c.cardid
                JOIN packages p ON c.packageid = p.packageid
                JOIN users u ON p.user_id = u.id
                WHERE t.id = @TradingId AND u.username = @Username;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@TradingId", tradingId);
                    command.Parameters.AddWithValue("@Username", username);

                    var result = (long)command.ExecuteScalar();
                    return result > 0; // True if the user is trying to trade with themselves
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in UserTryToTradeToHimselfRepo: {ex.Message}");
            return false; // Or handle the exception as per your error policy
        }
    }

    public void UpdateCardPackage(Guid cardId, Guid newPackageId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                UPDATE cards
                SET packageid = @NewPackageId
                WHERE cardid = @CardId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@NewPackageId", newPackageId);
                    command.Parameters.AddWithValue("@CardId", cardId);

                    command.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in UpdateCardPackage: {ex.Message}");
            // You might want to handle the exception more gracefully depending on your application's needs
        }
    }

    public Guid FindTheCardOfThatTradingIdRepo(Guid tradingId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT cardtotrade
                FROM trading
                WHERE id = @TradingId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@TradingId", tradingId);

                    var result = command.ExecuteScalar();
                    if (result != null && Guid.TryParse(result.ToString(), out Guid cardToTrade))
                    {
                        return cardToTrade; // Return the card ID if found
                    }

                    return Guid.Empty; // Return Guid.Empty if no card ID is found
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in FindTheCardOfThatTradingIdRepo: {ex.Message}");
            return Guid.Empty; // Return Guid.Empty or a specific error code in case of an exception
        }
    }



    public int FindUserIdWithGuidCard(Guid tradingId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT u.id
                FROM card c
                JOIN packages p ON c.packageid = p.packageid
                JOIN users u ON p.user_id = u.id
                WHERE c.cardid = @TradingId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@TradingId", tradingId);

                    var result = command.ExecuteScalar();
                    if (result != null && int.TryParse(result.ToString(), out int userId))
                    {
                        return userId; // Return the user ID if found
                    }

                    return 0; // Return 0 if no user ID is found
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in FindUserIdWithGuidCard: {ex.Message}");
            return 0; // Return 0 or a specific error code in case of an exception
        }
    }

    public Guid GetPackageIdByCardId(Guid cardId)
    {
        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
            SELECT p.packageid
            FROM cards c
            JOIN packages p ON c.packageid = p.packageid
            WHERE c.cardid = @CardId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@CardId", cardId);

                    var result = command.ExecuteScalar();
                    if (result != null && Guid.TryParse(result.ToString(), out Guid packageId))
                    {
                        return packageId; // Return the package ID if found
                    }

                    return Guid.Empty; // Return Guid.Empty if no package ID is found
                }
            }
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in GetPackageIdByCardId: {ex.Message}");
            return Guid.Empty; // Return Guid.Empty or handle the exception as per your error policy
        }
    }
    
    
    
    
    

    public List<Card> ListOfCardInPackageFromUser(int userId)
    {
        List<Card> cards = new List<Card>();

        try
        {
            using (var connection = _databaseConnection.GetConnection())
            {
                connection.Open();

                var query = @"
                SELECT c.cardid, c.cardtype, c.damage
                FROM cards c
                JOIN packages p ON c.packageid = p.packageid
                JOIN users u ON p.user_id = u.id
                WHERE u.id = @UserId;";

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@UserId", userId);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new Card(
                                reader.GetGuid(reader.GetOrdinal("cardid")),
                                reader.GetString(reader.GetOrdinal("cardtype")),
                                reader.GetDouble(reader.GetOrdinal("damage"))
                            );
                            cards.Add(card);
                        }
                    }
                }
            }

            return cards;
        }
        catch (Exception ex)
        {
            // Handle the exception (log it, etc.)
            Console.WriteLine($"Error in ListOfCardInPackageFromUser: {ex.Message}");
            return new List<Card>(); // Return an empty list or handle differently as per your error policy
        }
    }
}