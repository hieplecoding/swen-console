using MonsterTradingCards.Database;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Models.ModelsForGame;
using Npgsql;


namespace MonsterTradingCards.Repositories
{
    public class DeskRepository : IDeskRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public DeskRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public bool AddsUserDeck(int userId, List<Guid> cardIds)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        // Add new cards to the user's deck
                        for (int i = 0; i < cardIds.Count; i++)
                        {
                            var cardId = cardIds[i];
                            int slot = i + 1; // Assuming slot numbering starts at 1

                            var insertCommand =
                                new NpgsqlCommand(
                                    "INSERT INTO user_decks (UserId, CardId, Slot) VALUES (@UserId, @CardId, @Slot)",
                                    connection);
                            insertCommand.Parameters.AddWithValue("@UserId", userId);
                            insertCommand.Parameters.AddWithValue("@CardId", cardId);
                            insertCommand.Parameters.AddWithValue("@Slot", slot);
                            insertCommand.ExecuteNonQuery();
                        }

                        transaction.Commit();
                        return true;
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw; // Rethrow the exception for the caller to handle
                    }
                }
            }
        }

        public bool CheckIfUserHasCardInDeskRepo(string username, List<Guid> cardIds)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                using (var command = new NpgsqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"
                SELECT cards.cardid
                FROM cards
                INNER JOIN packages ON cards.packageid = packages.packageid
                INNER JOIN users ON packages.user_id = users.id
                WHERE cards.cardid = ANY(@CardIds)
                AND users.username = @Username;";

                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@CardIds", cardIds.ToArray());

                    using (var reader = command.ExecuteReader())
                    {
                        var foundCardIds = new HashSet<Guid>();
                        while (reader.Read())
                        {
                            foundCardIds.Add(reader.GetGuid(0));
                        }

                        // Check if all card IDs are found
                        return foundCardIds.Count == cardIds.Count && cardIds.All(id => foundCardIds.Contains(id));
                    }
                }
            }
        }

        public List<Card> GetCardInDeskByUsernameRepo(string username)
        {
            var cards = new List<Card>();

            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                //SELECT c.cardid, c.cardtype, c.damage 
                // FROM cards c
                // INNER JOIN packages p ON c.packageid = p.packageid
                // INNER JOIN users u ON p.user_id = u.id
                using (var command = new NpgsqlCommand(@"
            SELECT c.cardid, c.cardtype, c.damage
            FROM cards c
            INNER JOIN user_decks ud ON c.cardid = ud.cardid
            INNER JOIN users u ON ud.userid = u.id
            WHERE u.username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new Card(
                                reader.GetGuid(0),   // Id
                                reader.GetString(1), // Name
                                reader.GetDouble(2)  // Damage
                            );
                            cards.Add(card);
                        }
                    }
                }
            }

            return cards;
        }
    }
}