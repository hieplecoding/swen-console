using MonsterTradingCards.Models;
using MonsterTradingCards.Database;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Models.ModelsForGame;
using Npgsql;


namespace MonsterTradingCards.Repositories
{
    public class PackageRepository : IPackageRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public PackageRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public bool AddCards(List<Card> cards)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        // Create a new package and get its ID
                        var packageId = Guid.NewGuid();
                        using (var packageCommand =
                               new NpgsqlCommand("INSERT INTO Packages (PackageId) VALUES (@PackageId)", connection))
                        {
                            packageCommand.Parameters.AddWithValue("@PackageId", packageId);
                            packageCommand.ExecuteNonQuery();
                        }

                        // Add cards to the new package
                        foreach (var card in cards)
                        {
                            using (var cardCommand = new NpgsqlCommand(
                                       "INSERT INTO Cards (CardId, CardType, Damage, PackageId) VALUES (@CardId, @CardType, @Damage, @PackageId)",
                                       connection))
                            {
                                cardCommand.Parameters.AddWithValue("@CardId",
                                    card.Id == Guid.Empty ? Guid.NewGuid() : card.Id);
                                cardCommand.Parameters.AddWithValue("@CardType", card.Name);
                                cardCommand.Parameters.AddWithValue("@Damage", card.Damage);
                                cardCommand.Parameters.AddWithValue("@PackageId", packageId);

                                cardCommand.ExecuteNonQuery();
                            }
                        }

                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public bool AddPackageToUser(string username)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        int userId = GetUserIdByUsername(username, connection);
                        Guid packageId = GetPackageId(connection);

                        // Check if the package is available and user has sufficient coins in a single query
                        var checkQuery = @"
                            SELECT COUNT(*) FROM packages 
                            WHERE packageid = @PackageId AND user_id IS NULL
                            AND EXISTS (SELECT 1 FROM users WHERE id = @UserId AND money > 4)";
                        using (var checkCommand = new NpgsqlCommand(checkQuery, connection))
                        {
                            checkCommand.Parameters.AddWithValue("@UserId", userId);
                            checkCommand.Parameters.AddWithValue("@PackageId", packageId);
                            var availableAndAffordable = (long)checkCommand.ExecuteScalar() > 0;

                            if (!availableAndAffordable)
                            {
                                transaction.Rollback();
                                return false;
                            }
                        }

                        // Assign package to user and deduct coins
                        if (AssignPackageToUser(packageId, userId, connection))
                        {
                            transaction.Commit();
                            return true;
                        }
                        else
                        {
                            transaction.Rollback();
                            return false;
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public bool CheckPackageToBuy()
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var command = new NpgsqlCommand("SELECT COUNT(*) FROM packages", connection))
                {
                    var count = (long)command.ExecuteScalar();
                    return count > 0; // Return true if there are records in the packages table, false otherwise
                }
            }
        }

        public bool checkMoney(string username)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                using (var command = new NpgsqlCommand(
                           "SELECT money FROM users WHERE username = @Username",
                           connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
            
                    var result = command.ExecuteScalar();

                    if (result != null && result != DBNull.Value)
                    {
                        int money = Convert.ToInt32(result);
                        return money >= 5;
                    }

                    return false;
                }
            }
        }


        // Helper Methods

        private int GetUserIdByUsername(string username, NpgsqlConnection connection)
        {
            using (var command = new NpgsqlCommand("SELECT id FROM users WHERE username = @Username", connection))
            {
                command.Parameters.AddWithValue("@Username", username);
                var result = command.ExecuteScalar();
                return (result != null) ? Convert.ToInt32(result) : 0;
            }
        }

        private Guid GetPackageId(NpgsqlConnection connection)
        {
            using (var command = new NpgsqlCommand(
                       "SELECT packageid FROM packages WHERE user_id IS NULL ORDER BY sequence_number LIMIT 1",
                       connection))
            {
                var result = command.ExecuteScalar();
                return (result != null) ? (Guid)result : Guid.Empty;
            }
        }




        private bool AssignPackageToUser(Guid packageId, int userId, NpgsqlConnection connection)
        {
            using (var command = new NpgsqlCommand("UPDATE packages SET user_id = @UserId WHERE packageid = @PackageId",
                       connection))
            {
                command.Parameters.AddWithValue("@UserId", userId);
                command.Parameters.AddWithValue("@PackageId", packageId);

                if (command.ExecuteNonQuery() == 1)
                {
                    // Deduct coins
                    return DeductCoins(userId, connection);
                }

                return false;
            }
        }

        private bool DeductCoins(int userId, NpgsqlConnection connection)
        {
            using (var command = new NpgsqlCommand("UPDATE users SET money = money - 5 WHERE id = @UserId", connection))
            {
                command.Parameters.AddWithValue("@UserId", userId);
                return command.ExecuteNonQuery() == 1;
            }
        }

    }
}