using MonsterTradingCards.Database;
using MonsterTradingCards.Interface.Stats;
using MonsterTradingCards.Models;
using MonterTradingCards.Models;
using Npgsql;

namespace MonsterTradingCards.Repositories;

public class StatsRepository: IStatsRepository
{
    private readonly DatabaseConnection _dbConnection;

    public StatsRepository(DatabaseConnection dbConnection)
    {
        _dbConnection = dbConnection;
    }

    public UserStats? GetUserStatsByUsername(string username)
    {
        try
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var command = new NpgsqlCommand("SELECT username, elo, wins, losses FROM users WHERE username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new UserStats
                            {
                                Name = reader["username"].ToString(),
                                Elo = Convert.ToInt32(reader["elo"]),
                                Wins = Convert.ToInt32(reader["wins"]),
                                Losses = Convert.ToInt32(reader["losses"])
                            };
                        }
                    }
                }
            }
            return null; // Return null if user not found
        }
        catch (NpgsqlException ex)
        {
            // Log exception and handle it appropriately
            Console.WriteLine($"Error in GetUserStatsByUsername: {ex.Message}");
            return null;
        }
    }
}
