using MonsterTradingCards.Database;
using MonsterTradingCards.Interface.ScoreBoard;
using MonsterTradingCards.Models;
using MonterTradingCards.Models;
using Npgsql;

namespace MonsterTradingCards.Repositories
{
    public class ScoreBoardRepository : IScoreBoardRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public ScoreBoardRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public List<UserStats> GetUserStatsByUserElo()
        {
            List<UserStats> userStatsList = new List<UserStats>(); // Initialize a list to hold the UserStats objects

            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    using (var command = new NpgsqlCommand("SELECT username, elo, wins, losses FROM users", connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read()) // Iterate over all rows in the result set
                            {
                                var userStats = new UserStats
                                {
                                    Name = reader["username"].ToString(),
                                    Elo = Convert.ToInt32(reader["elo"]),
                                    Wins = Convert.ToInt32(reader["wins"]),
                                    Losses = Convert.ToInt32(reader["losses"])
                                };
                                userStatsList.Add(userStats); // Add the userStats object to the list
                            }
                        }
                    }
                }

                return userStatsList; // Return the list of UserStats
            }
            catch (NpgsqlException ex)
            {
                // Log exception and handle it appropriately
                Console.WriteLine($"Error in GetUserStatsByUserElo: {ex.Message}");
                return
                    userStatsList; // Return the list even in case of an exception (it will be empty if no data was fetched)
            }
        }
    }
}