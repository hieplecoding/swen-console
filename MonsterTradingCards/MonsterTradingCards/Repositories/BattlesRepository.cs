using System.Data;
using System.Data.Common;
using MonsterTradingCards.Database;
using MonsterTradingCards.Interface.Battles;
using MonsterTradingCards.Interface.Database;
using MonsterTradingCards.Models.ModelsForGame;
using Npgsql;

namespace MonsterTradingCards.Repositories;

public class BattlesRepository: IBattlesRepository
{
    private readonly IDatabaseConnection _dbConnection;
        

    public BattlesRepository(IDatabaseConnection dbConnection)
    {
        // only created in constructor and unchanged
        _dbConnection = dbConnection;
            
    }
    public void ChangeStatsAfterBattle(string userWin, string userLose)
    {
        try
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                // Update the winning user
                using (var winCommand = new NpgsqlCommand("UPDATE users SET elo = elo + 3, wins = wins + 1 WHERE username = @Username", connection))
                {
                    winCommand.Parameters.AddWithValue("@Username", userWin);
                    winCommand.ExecuteNonQuery();
                }

                // Update the losing user
                using (var loseCommand = new NpgsqlCommand("UPDATE users SET elo = elo - 5, losses = losses + 1 WHERE username = @Username", connection))
                {
                    loseCommand.Parameters.AddWithValue("@Username", userLose);
                    loseCommand.ExecuteNonQuery();
                }
            }
        }
        catch (NpgsqlException ex)
        {
            // Log exception and handle it appropriately
            Console.WriteLine($"Error in ChangeStatsAfterBattle: {ex.Message}");
        }
    }

    public void RemoveLoseCardFromDeckRepo(Card card)
    {
        using (var connection = _dbConnection.GetConnection())
        {
            connection.Open();

            // SQL command to remove the card from the trading table
            using (var command = new NpgsqlCommand(@"
                DELETE FROM trading 
                WHERE cardtotrade = @CardId", connection))
            {
                // Use the card's GUID ID as the parameter for the SQL command
                command.Parameters.AddWithValue("@CardId", card.Id);

                // Execute the command
                int rowsAffected = command.ExecuteNonQuery();

                // You can use rowsAffected to check if the delete operation was successful
                if (rowsAffected == 0)
                {
                    // Handle the case where no row was deleted
                    // This might happen if there's no card with the given ID in the trading table
                    Console.WriteLine("No card was removed. ");
                }
            }
        }
    }

    public int CountCardInDeckRepo(string username)
    {
        using (var connection = _dbConnection.GetConnection())
        {
            connection.Open();

            using (var command = new NpgsqlCommand(@"
                SELECT COUNT(*)
                FROM user_decks
                INNER JOIN users ON user_decks.userid = users.id
                WHERE users.username = @Username", connection))
            {
                command.Parameters.AddWithValue("@Username", username);

                // Execute the command and return the card count
                return Convert.ToInt32(command.ExecuteScalar());
            }
        }
    }
}