﻿using MonsterTradingCards.Database;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Interface.Stats;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;
using MonterTradingCards.Models;
using Npgsql;




namespace MonsterTradingCards.Repositories
{
    public class UserRepository : IUserRepository
    {
        // readdonly is assigned only one and remains unchanged
        // This preserves the integrity of the data.
        private readonly DatabaseConnection _dbConnection;
        

        public UserRepository(DatabaseConnection dbConnection)
        {
            // only created in constructor and unchanged
            _dbConnection = dbConnection;
            
        }
        public bool TestDatabaseConnection()
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open(); // Attempt to open the connection
                    return connection.State == System.Data.ConnectionState.Open; // Check if the connection is open
                }
            }
            catch (NpgsqlException ex)
            {
                Console.WriteLine($"Error connecting to the database: {ex.Message}");
                return false;
            }
        }
        
        // User
        public bool UserExists(string username)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                using (var command = new NpgsqlCommand("SELECT COUNT(*) FROM users WHERE username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@username", username);

                    var result = (long)command.ExecuteScalar();
                    return result > 0;
                }
            }
        }

        public void AddUser(UserCredentials user)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                // Begin a transaction
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        // Hash the password
                        var hashedPassword = BCrypt.Net.BCrypt.HashPassword(user.Password);

                        // Insert into Users table with all fields
                        using (var command = new NpgsqlCommand("INSERT INTO Users (Username, Password, Name, Bio, Image, Money) VALUES (@Username, @Password, @Name, @Bio, @Image, @Money) RETURNING Id", connection, transaction))
                        {
                            command.Parameters.AddWithValue("@Username", user.Username);
                            command.Parameters.AddWithValue("@Password", hashedPassword);
                            command.Parameters.AddWithValue("@Name", "Hoax");
                            command.Parameters.AddWithValue("@Bio", "me playin...");
                            command.Parameters.AddWithValue("@Image", ":-)");
                            command.Parameters.AddWithValue("@Money", 20);
                            // Execute the command and get the generated Id
                            var userId = (int)command.ExecuteScalar();

                            // Commit the transaction
                            transaction.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        // Handle exceptions and optionally roll back the transaction
                        transaction.Rollback();
                        throw; // Rethrow the exception
                    }
                }
            }
        }


        public UserCredentials? GetUserByUsername(string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    using (var command = new NpgsqlCommand("SELECT username FROM users WHERE username = @Username", connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return new UserCredentials
                                {
                                    Username = reader["username"].ToString()
                                    // Set other properties from the reader
                                };
                            }
                        }
                    }
                }
                return null; // Return null if user not found
            }
            catch (NpgsqlException ex)
            {
                // Log exception and handle it appropriately
                // Maybe log the error and return null or rethrow, depending on your application's needs
                Console.WriteLine($"Error in GetUserByUsername: {ex.Message}");
                return null;
            }
        }


        public UserData? UpdateUserDataByUsername(string username, UserData updatedUserData)
            {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        // Update the corresponding user record in the Users table
                        using (var updateUserDataCommand = new NpgsqlCommand("UPDATE Users SET Name = @Name, Bio = @Bio, Image = @Image WHERE username = @Username RETURNING Id", connection, transaction))
                        {
                            updateUserDataCommand.Parameters.AddWithValue("@Name", updatedUserData.Name);
                            updateUserDataCommand.Parameters.AddWithValue("@Bio", updatedUserData.Bio);
                            updateUserDataCommand.Parameters.AddWithValue("@Image", updatedUserData.Image);
                            updateUserDataCommand.Parameters.AddWithValue("@Username", username); // Set the @Username parameter

                            var userId = updateUserDataCommand.ExecuteScalar();

                            if (userId != null)
                            {
                                // Commit the transaction
                                transaction.Commit();

                                return updatedUserData; // Return the updated UserData
                            }
                        }

                        return null; // User not found
                    }
                    catch (Exception ex)
                    {
                        // Handle exceptions appropriately (e.g., log the error)
                        transaction.Rollback();
                        return null; // Return null on error
                    }
                }
            }
        }

        // sessions
        public bool LoginUser(string username, string password)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                using (var command = new NpgsqlCommand("SELECT password FROM users WHERE username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);

                    var result = command.ExecuteScalar();

                    if (result != null)
                    {
                        // Assuming the password in the database is hashed
                        // Compare the hashed password from the database with the provided password
                        string hashedPasswordFromDb = result.ToString();
                        return VerifyPassword(password, hashedPasswordFromDb);
                    }
                }
            }
            return false;
        }

        private bool VerifyPassword(string plainPassword, string hashedPassword)
        {
            bool result = BCrypt.Net.BCrypt.Verify(plainPassword, hashedPassword);
            return result;
        }
        
        // help function
        public int GetUserId(string username)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var command = new NpgsqlCommand("SELECT id FROM users WHERE username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);

                    var result = command.ExecuteScalar();
                    if (result != null)
                    {
                        return (int)result; // Cast the result to integer and return
                    }
                    else
                    {
                        throw new InvalidOperationException("User not found.");
                    }
                }
            }
        }

        public User? GetUserAndCardInDeck(string username)
        {
            //int userId = GetUserId(username);
            List<Guid> cardIds = GetCardInDeskByUsernameRepo(username);
            List<Card?> deck = new List<Card?>();

            foreach (var cardId in cardIds)
            {
                // Assuming you have a method to get a Card by its ID
                Card? card = GetCardById(cardId); 
                deck.Add(card);
            }

            // Assuming you have a method to get UserStats by userID
            UserStats? stats = GetUserStatsByUsername(username);

            // Ensure the deck array has the correct size (4 in this case)
            Card?[] deckArray = deck.Take(4).ToArray();

            return new User(username, deckArray, stats);
        }

        public List<Guid> GetCardInDeskByUsernameRepo(string username)
        {
            List<Guid> cardIds = new List<Guid>();

            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                using (var command = new NpgsqlCommand(@"
            SELECT ud.cardid
            FROM user_decks ud
            INNER JOIN users u ON ud.userid = u.id
            WHERE u.username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cardIds.Add(reader.GetGuid(0));
                        }
                    }
                }
            }

            return cardIds;
        }

        public UserStats? GetUserStatsByUsername(string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    using (var command = new NpgsqlCommand("SELECT username, elo, wins, losses FROM users WHERE username = @Username", connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return new UserStats
                                {
                                    Name = reader["username"].ToString(),
                                    Elo = Convert.ToInt32(reader["elo"]),
                                    Wins = Convert.ToInt32(reader["wins"]),
                                    Losses = Convert.ToInt32(reader["losses"])
                                };
                            }
                        }
                    }
                }
                return null; // Return null if user not found
            }
            catch (NpgsqlException ex)
            {
                // Log exception and handle it appropriately
                Console.WriteLine($"Error in GetUserStatsByUsername: {ex.Message}");
                return null;
            }
        }

        public Card? GetCardById(Guid cardId)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();

                using (var command = new NpgsqlCommand("SELECT cardtype, damage FROM cards WHERE cardid = @CardId", connection))
                {
                    command.Parameters.AddWithValue("@CardId", cardId);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            string cardType = reader.GetString(0);
                            double damage = reader.GetDouble(1);

                            // Assuming Card class has a constructor that takes a CardType and damage
                            return new Card(cardId, cardType, damage);
                        }
                    }
                }
            }

            throw new InvalidOperationException("Card not found.");
        }

    }
}