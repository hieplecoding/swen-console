﻿using System.Data;
using Npgsql;// to work with Postgre
using System.Text.Json;
using System.IO;
using MonsterTradingCards.Interface.Database;

namespace MonsterTradingCards.Database
{
    public class DatabaseConnection: IDatabaseConnection
    {
        private readonly string _connectionString = "Host=127.0.0.1;Port=5432;Username=postgres;Password=postgres;Database=mydatabase";

        public NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(_connectionString);
        }
    }
}
