﻿using System.Net.Sockets;

// Assuming this namespace contains User, Card, etc.

namespace MonsterTradingCards.Models.ModelsForGame
{
    public class Game
    {
        private List<TcpClient> _clients = new List<TcpClient>();
        private ManualResetEvent _lobbyDoneEvent = new ManualResetEvent(false);

        private User? User1 = null;
        private User? User2 = null;

        public object LobbyFull { get; private set; } = false;
        private List<Card?> Team1 { get; set; }
        private List<Card?> Team2 { get; set; }

        private int RoundCount { get; set; }

        public Game()
        {
            Team1 = new List<Card?>();
            Team2 = new List<Card?>();
            RoundCount = 0;
        }

        public void EnterLobby(User user, TcpClient client)
        {
            lock (_clients)
            {
                if (_clients.Count == 0)
                {
                    _clients.Add(client);
                    User1 = user;
                    Console.WriteLine("Player 1 Entered");
                }
                else if (_clients.Count == 1)
                {
                    lock (LobbyFull)
                    {
                        LobbyFull = true;
                    }
                    _clients.Add(client);
                    User2 = user;
                    Console.WriteLine("Player 2 Entered");

                    // Initialize teams
                    Team1 = User1.Deck.ToList();
                    Team2 = User2.Deck.ToList();

                    // Start the game in a new thread
                    Task.Run(() => StartGame());
                }
            }
        }

        public void WaitsForGame()
        {
            _lobbyDoneEvent.WaitOne();
        }

        private void StartGame()
        {
            while (!IsGameOver())
            {
                ExecuteRound();
            }

            string outcome = DetermineOutcome();
            Console.WriteLine(outcome);
            _lobbyDoneEvent.Set();
        }

        private bool IsGameOver()
        {
            return Team1.Count == 0 || Team2.Count == 0 || RoundCount >= 100;
        }

        public void ExecuteRound()
        {
            if (Team1.Any() && Team2.Any())
            {
                Card? team1Card = Team1[0];
                Card? team2Card = Team2[0];

                Console.Write($"PlayerA: {team1Card.Name} ({team1Card.Damage} Damage) vs PlayerB: {team2Card.Name} ({team2Card.Damage} Damage) => ");
                
                var (outcome1, effectiveDamage1, opponentEffectiveDamage1) = team1Card.Attack(team2Card);
                var (outcome2, effectiveDamage2, opponentEffectiveDamage2) = team2Card.Attack(team1Card);

                Console.Write($"{effectiveDamage1} vs {effectiveDamage2} => ");

                HandleBattleOutcome(team1Card, team2Card, outcome1, outcome2);

                RoundCount++;
            }
        }

        private void HandleBattleOutcome(Card? team1Card, Card? team2Card, int team1Result, int team2Result)
        {
            if (team1Result == 1 && team2Result != 1)
            {
                Console.WriteLine($"{team1Card.Name} wins");
                Team2.RemoveAt(0);
            }
            else if (team2Result == 1 && team1Result != 1)
            {
                Console.WriteLine($"{team2Card.Name} wins");
                Team1.RemoveAt(0);
            }
            else
            {
                Console.WriteLine("The battle is a draw");
            }
        }

        public string DetermineOutcome()
        {
            if (Team1.Count > Team2.Count)
            {
                return "Team 1 Wins";
            }

            if (Team2.Count > Team1.Count)
            {
                return "Team 2 Wins";
            }

            return "Draw";
        }
    }
}
