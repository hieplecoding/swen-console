namespace MonsterTradingCards.Models.ModelsForGame;

public enum CardType
{
    WaterGoblin = 1,
    FireGoblin = 2,
    RegularGoblin = 3,
    WaterTroll = 4,
    FireTroll = 5,
    RegularTroll = 6,
    WaterElf = 7,
    FireElf = 8,
    RegularElf = 9,
    WaterSpell = 10,
    FireSpell = 11,
    RegularSpell = 12,
    Knight = 13,
    Dragon = 14,
    Ork = 15,
    Kraken = 16
}