﻿namespace MonsterTradingCards.Models.ModelsForGame
{
    public class Card
    {
        public Guid Id { get; set; }
        public string Name { get; set; } // Keep as string
        public double Damage { get; set; }

        public Card(Guid id, string name, double damage)
        {
            Id = id;
            Name = name; // Assign directly as a string
            Damage = damage;
        }

        

        public virtual (int outcome, double effectiveDamage, double opponentEffectiveDamage) Attack(Card? enemy)
        {
            return (0, 0, 0);
        }
    }
}