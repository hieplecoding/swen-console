﻿namespace MonsterTradingCards.Models.ModelsForGame
{
    
    public class Monster : Card
    {
       

        public CardType Type { get; private set; }

        public Monster(Guid id, CardType type, double damage) : base(id, type.ToString(), damage)
        {
            Type = type;
        }

        // 1 is win , 2 is lose, 3 is draw

        public override (int outcome, double effectiveDamage, double opponentEffectiveDamage) Attack(Card? opponent)
        {
            int thisCardType = (int)this.Type;
            int opponentCardType = (int)Enum.Parse<CardType>(opponent.Name);

            double effectiveDamage = this.Damage;
            double opponentEffectiveDamage = opponent.Damage;

            // Special interactions based on numeric values
            if (thisCardType is >= 1 and <= 3 && opponentCardType == 14) // Goblins vs Dragon
            {
                Console.WriteLine("Goblins are afraid of Dragons");
                return (2, effectiveDamage, opponentEffectiveDamage); // Goblins lose
            }

            if (thisCardType == 13 && opponentCardType == 10) // Knight vs Water Spell
            {
                Console.WriteLine("Knight is drowning");
                return (2, effectiveDamage, opponentEffectiveDamage); // Knight loses
            }

            if (thisCardType >= 7 && thisCardType <= 9 && opponentCardType == 14) // FireElves vs Dragon
            {
                Console.WriteLine("Fire Elves evade the attack from Dragon");
                return (3, effectiveDamage, opponentEffectiveDamage); // Draw
            }

            // Regular attack logic
            int outcome = effectiveDamage > opponentEffectiveDamage ? 1 :
                effectiveDamage < opponentEffectiveDamage ? 2 : 3;
            return (outcome, effectiveDamage, opponentEffectiveDamage);
        }


        /*
        public override void Attack(Card opponent)
        {
            throw new NotImplementedException();
        }
        */
    }
}