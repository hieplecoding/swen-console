﻿namespace MonsterTradingCards.Models.ModelsForGame
{
    public class Spell : Card
    {
        public CardType Type { get; private set; } // Include the CardType enum

        public Spell(Guid id, CardType type, double damage) : base(id, type.ToString(), damage)
        {
            Type = type; // Set the enum
        }

        public override (int outcome, double effectiveDamage, double opponentEffectiveDamage) Attack(Card? opponent)
        {
            double effectiveDamage = this.Damage;
            double opponentEffectiveDamage = opponent.Damage;

            // Assuming opponent's Type is accessible. Otherwise, you'll need to parse it from the Name.
            CardType opponentType = Enum.Parse<CardType>(opponent.Name); 

            // Kraken can defeat every spell
            if (opponentType == CardType.Kraken)
            {
                return (2, effectiveDamage, opponentEffectiveDamage); // Spell loses
            }

            // Determine effectiveness based on spell and enemy types
            switch (Type)
            {
                case CardType.WaterSpell:
                    if (IsFireType(opponentType))
                    {
                        effectiveDamage *= 2;
                        opponentEffectiveDamage /= 2;
                    }
                    else if (IsRegularType(opponentType))
                    {
                        effectiveDamage /= 2;
                        opponentEffectiveDamage *= 2;
                    }
                    break;

                case CardType.FireSpell:
                    if (IsRegularType(opponentType))
                    {
                        effectiveDamage *= 2;
                        opponentEffectiveDamage /= 2;
                    }
                    else if (IsWaterType(opponentType))
                    {
                        effectiveDamage /= 2;
                        opponentEffectiveDamage *= 2;
                    }
                    break;

                case CardType.RegularSpell:
                    if (IsWaterType(opponentType))
                    {
                        effectiveDamage *= 2;
                        opponentEffectiveDamage /= 2;
                    }
                    else if (IsFireType(opponentType))
                    {
                        effectiveDamage /= 2;
                        opponentEffectiveDamage *= 2;
                    }
                    break;
            }

            // Determine the outcome
            int outcome = effectiveDamage > opponentEffectiveDamage ? 1 : effectiveDamage < opponentEffectiveDamage ? 2 : 3;
            return (outcome, effectiveDamage, opponentEffectiveDamage);
        }

        private bool IsFireType(CardType type)
        {
            return type == CardType.FireGoblin || type == CardType.FireTroll || type == CardType.FireSpell || type == CardType.FireElf; // Fire types
        }

        private bool IsWaterType(CardType type)
        {
            return type == CardType.WaterGoblin || type == CardType.WaterTroll || type == CardType.WaterSpell || type == CardType.WaterElf; // Water types
        }

        private bool IsRegularType(CardType type)
        {
            return type == CardType.RegularGoblin || type == CardType.RegularTroll || type == CardType.RegularSpell || type == CardType.RegularElf; // Regular types
        }
    }
}
