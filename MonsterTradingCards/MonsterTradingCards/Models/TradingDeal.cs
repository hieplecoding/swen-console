﻿

namespace MonterTradingCards.Models
{
    public class TradingDeal
    {
        public Guid Id { get; set; }
        public Guid CardToTrade { get; set; }
        public string? Type { get; set; } // "monster" or "spell"
        public double MinimumDamage { get; set; }
    }
    public class TradingInfo
    {
        public Guid Id { get; set; }
    }
}
