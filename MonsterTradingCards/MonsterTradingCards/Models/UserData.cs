namespace MonsterTradingCards.Models;

public class UserData
{
    public int Id{get; set; }
    public string? Name { get; set; }
    public string? Bio { get; set; }
    public string? Image { get; set; }
    
}