using MonsterTradingCards.Interface.Trading;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Services;

public class TradingService: ITradingService
{
    private readonly ITradingRepository _tradingRepository;

    public TradingService(ITradingRepository tradingRepository)
    {
        _tradingRepository = tradingRepository;
    }
    public List<TradingDeal> GetTradingDealService(string username)
    {
        return _tradingRepository.GetTradingDealRepository(username);
    }

    public bool CheckCardIfUserNotOwnOrInDeckService(TradingDeal? tradingDeal,string username)
    {
        return _tradingRepository.CheckCardIfUserNotOwnOrInDeckRepo(tradingDeal, username);
    }

    public bool CheckDealIsAlreadyExistService(TradingDeal? tradingDeal, string username)
    {
        return _tradingRepository.CheckDealIsAlreadyExistRepo(tradingDeal, username);
    }

    public void CreateTradingService(TradingDeal? tradingDeal)
    {
        _tradingRepository.CreateTradingRepo(tradingDeal);
    }

    public bool UserHasDealService(Guid tradingId, string username)
    {
        return _tradingRepository.UserHasDealRepository(tradingId, username);
    }

    public bool FindDealService(Guid tradingId)
    {
        return _tradingRepository.FindDealRepo(tradingId);
    }

    public void DeleteTradingService(Guid tradingId)
    {
        _tradingRepository.DeleteTradingRepo(tradingId);
    }

    public bool IfCardIsGoodEnoughServiceOrNotYourOwnCardService(Guid tradingId, string username)
    {
        return _tradingRepository.IfCardIsGoodEnoughServiceOrNotYourOwnCardRepo(tradingId, username);
    }

    public void DoTradingService(Guid tradingIdHas, Guid tradingIdWantToTrade, Guid idToDelete)
    {
        _tradingRepository.DoTradingRepo( tradingIdHas, tradingIdWantToTrade, idToDelete);
    }

    public Guid FindTheCardOfThatTradingIdService(Guid tradingId)
    {
        return _tradingRepository.FindTheCardOfThatTradingIdRepo(tradingId);
    }

    public bool UserTryToTradeToHimSelfService(Guid tradingId, string username)
    {
        return _tradingRepository.UserTryToTradeToHimSelfRepo(tradingId, username);
    }

    public bool CheckCardOwnership(string username, Guid cardId)
    {
        return _tradingRepository.CheckCardOwnership(username, cardId);
    }

    public bool IsCardInDeck(string username, Guid cardId)
    {
        return _tradingRepository.IsCardInDeck(username, cardId);
    }
}