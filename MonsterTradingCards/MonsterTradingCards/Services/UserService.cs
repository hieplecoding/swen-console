﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Models;

using MonterTradingCards.Models;


namespace MonsterTradingCards.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// post request to register and add username, password in database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool RegisterUser(UserCredentials user)
        {
            if (user.Username != null)
            {
                if (_userRepository.UserExists(user.Username))
                {
                    return false; // User already exists
                }

                _userRepository.AddUser(user);
                return true; // User successfully registered
            }

            return false;
        }
        /// <summary>
        /// To search for user by search username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public UserCredentials? RetrieveUserData(string username)
        {
            return _userRepository.GetUserByUsername(username);
        }

        public UserData? UpdateUserData(string username, UserData updatedUserData)
        {
            return _userRepository.UpdateUserDataByUsername(username, updatedUserData);
        }

        /// <summary>
        /// Login user with username and password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool LoginUser(UserCredentials user)
        {
            if (user.Username != null && user.Password != null)
            {
                if (_userRepository.LoginUser(user.Username, user.Password))
                {
                    return true; // User already exists
                }

                return false;
            }

            return false;
        }
        
        // generate token to remember username
        public string GenerateToken(string username)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("OEJlZSSg129hkieMLHQa4-7cRZMUgCtFKtZgOjeOb_Q"); // Replace with a secure key

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, username)
                }),
                Expires = DateTime.UtcNow.AddDays(1), // Set token expiry
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public bool ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("OEJlZSSg129hkieMLHQa4-7cRZMUgCtFKtZgOjeOb_Q");
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true, // Ensure this is set to true to validate the signature
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true,
                // If you're not using 'kid' claim in your token headers, make sure there's no requirement for it in the validation process.
            };

            try
            {
                SecurityToken validatedToken;
                tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
                return true;
            }
            catch (SecurityTokenSignatureKeyNotFoundException ex)
            {
                // Log or handle the exception if necessary
                // This exception is thrown if the signature key could not be found
                return false;
            }
            catch (SecurityTokenException ex)
            {
                // Log or handle the exception if necessary
                // This exception is thrown for other token-related errors
                return false;
            }
        }

        public User? GetUserAndCardInDeck(string username)
        {
            return _userRepository.GetUserAndCardInDeck(username);
        }
    }
}
