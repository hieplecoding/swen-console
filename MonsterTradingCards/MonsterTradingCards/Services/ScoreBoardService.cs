using MonsterTradingCards.Interface.ScoreBoard;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Services;

public class ScoreBoardService: IScoreBoardService
{
    private readonly IScoreBoardRepository _scoreBoardRepository;

    public ScoreBoardService(IScoreBoardRepository scoreBoardRepository)
    {
        _scoreBoardRepository = scoreBoardRepository;
    }
    
    public List<UserStats> GetUserStats()
    {
        return _scoreBoardRepository.GetUserStatsByUserElo();
    }
}