using MonsterTradingCards.Interface;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Services;

public class DeskService: IDeskService
{
    private readonly IDeskRepository _deskRepository;
   
    private readonly IUserRepository _userRepository;

    public DeskService(IDeskRepository deskRepository, IUserRepository userRepository)
    {
        _deskRepository = deskRepository;
        
        _userRepository = userRepository;
    }
    public List<Card> GetCardInDeskByUsernameService(string username)
    {
        return _deskRepository.GetCardInDeskByUsernameRepo(username);
    }

    

    public bool PutCardInDesk(string username, List<Guid> cardIds)
    {

        if (string.IsNullOrEmpty(username) || cardIds == null || cardIds.Count != 4)
        {
            throw new ArgumentException("Invalid arguments for updating the desk.");
        }

        
        int userid = _userRepository.GetUserId(username);
        

        return _deskRepository.AddsUserDeck(userid, cardIds);
    }

    public bool CheckIfUserHasCard(string username, List<Guid> cardIds)
    {
        return _deskRepository.CheckIfUserHasCardInDeskRepo(username, cardIds);
    }
}