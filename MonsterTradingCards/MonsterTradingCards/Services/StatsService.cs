using MonsterTradingCards.Interface.Stats;
using MonsterTradingCards.Models;
using MonterTradingCards.Models;

namespace MonsterTradingCards.Services;

public class StatsService: IStatsService
{
    private readonly IStatsRepository _statsRepository;

    public StatsService(IStatsRepository statsRepository)
    {
        _statsRepository = statsRepository;
    }
    
    public UserStats? GetUserStats(string username)
    {
        return _statsRepository.GetUserStatsByUsername(username);
    }
}