using MonsterTradingCards.Interface;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Services;

public class CardService: ICardService
{
    private readonly ICardRepository _cardRepository;

    public CardService(ICardRepository cardRepository)
    {
        _cardRepository = cardRepository;
    }
    
    public List<Card> GetAllCardsForUser(string username)
    {
        return _cardRepository.GetCardsByUsername(username);
    }
}