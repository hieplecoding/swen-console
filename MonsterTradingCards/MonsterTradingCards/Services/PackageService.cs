using MonsterTradingCards.Interface;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;


namespace MonsterTradingCards.Services;

public class PackageService: IPackageService
{
    private readonly IPackageRepository _packageRepository;

    public PackageService(IPackageRepository packageRepository)
    {
        _packageRepository = packageRepository;
    }

    public bool CreatePackage(List<Card> cards)
    {
        if (cards.Count == 0)
        {
            return false;
        }

        // Call the repository method to save these cards to the database
        return _packageRepository.AddCards(cards);
    }

    public bool BuyPackage(string username)
    {


        
        return _packageRepository.AddPackageToUser(username);
    }

    

    public bool CheckPackage()
    {
        if (!_packageRepository.CheckPackageToBuy())
        {
            return false;
        }

        return true;
    }

    public bool checkMoney(string username)
    {

        if (!_packageRepository.checkMoney(username))
        {
            return false;
        }

        return true;
    }
}