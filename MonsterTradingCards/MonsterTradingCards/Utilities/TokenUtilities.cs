using System.IdentityModel.Tokens.Jwt;

namespace MonsterTradingCards.Utilities;

public class TokenUtilities
{
    public static string DecodeTokenUsername(string token)
    {
        var handler = new JwtSecurityTokenHandler();
        var jsonToken = handler.ReadToken(token) as JwtSecurityToken;

        var usernameClaim = jsonToken?.Claims.FirstOrDefault(claim => claim.Type == "unique_name");

        return usernameClaim?.Value;
    }
}