using MonsterTradingCards.Models.ModelsForGame;

namespace MonsterTradingCards.Utilities;

public class CardFactory
{
    public Card CreateCard(Guid id, string name, double damage)
    {
        switch (name)
        {
            case "WaterSpell":
            case "FireSpell":
            case "NormalSpell":
                return new Spell(id, ParseCardType(name), damage);

            default:
                return new Monster(id, ParseCardType(name), damage);
        }
    }

    private CardType ParseCardType(string name)
    {
        // Assuming Enum.TryParse can handle the conversion from name to CardType.
        // Make sure the name matches exactly with the enum value.
        if (Enum.TryParse<CardType>(name, out CardType cardType))
        {
            return cardType;
        }

        throw new ArgumentException($"Invalid card name: {name}");
    }
}
