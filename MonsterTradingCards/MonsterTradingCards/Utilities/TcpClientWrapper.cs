using System.Net.Sockets;

namespace MonsterTradingCards.Utilities;
public interface ITcpClientWrapper
{
    NetworkStream GetStream();
    void Close();
    // Add other methods you use from TcpClient
}
public class TcpClientWrapper : ITcpClientWrapper
{
    private readonly TcpClient _tcpClient;

    public TcpClientWrapper(TcpClient tcpClient)
    {
        _tcpClient = tcpClient;
    }

    public NetworkStream GetStream()
    {
        return _tcpClient.GetStream();
    }

    public void Close()
    {
        _tcpClient.Close();
    }
}