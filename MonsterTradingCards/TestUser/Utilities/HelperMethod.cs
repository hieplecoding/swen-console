using MonsterTradingCards.Database;
using MonsterTradingCards.Repositories;
using MonsterTradingCards.Services;
using MonterTradingCards.Models;
using Npgsql;
namespace TestUser.Utilities;

public class HelperMethod
{
    private UserService _userService;
    private UserRepository _userRepository;
    private DatabaseConnection _databaseConnection;

    public HelperMethod()
    {
        // Initialize the database connection
        _databaseConnection = new DatabaseConnection();

        // Initialize the repository with the database connection
        _userRepository = new UserRepository(_databaseConnection);

        // Initialize the service with the repository
        _userService = new UserService(_userRepository);
    }
    public void AddTestUserToDatabase(string username, string password)
    {
        using (var connection = _databaseConnection.GetConnection())
        {
            connection.Open();
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(password); // Hash the password

            using (var command = new NpgsqlCommand("INSERT INTO users (Username, Password) VALUES (@Username, @Password)", connection))
            {
                command.Parameters.AddWithValue("@Username", username);
                command.Parameters.AddWithValue("@Password", hashedPassword);
                command.ExecuteNonQuery();
            }
        }
    }
        
    public void DeleteTestUserFromDatabase(string username)
    {
        using (var connection = _databaseConnection.GetConnection())
        {
            connection.Open();
            using (var command = new NpgsqlCommand("DELETE FROM users WHERE Username = @Username", connection))
            {
                command.Parameters.AddWithValue("@Username", username);
                command.ExecuteNonQuery();
            }
        }
    }
}