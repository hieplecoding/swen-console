using Moq;
using NUnit.Framework;
using MonsterTradingCards.Repositories;
using MonsterTradingCards.Models.ModelsForGame;
using System;
using System.Data;
using MonsterTradingCards.Database;
using MonsterTradingCards.Interface.Database;
using MonterTradingCards.Models;
using Npgsql;

namespace TestUser
{
    [TestFixture]
    public class TradingRepositoryTests
    {
        private DatabaseConnection _databaseConnection;
        private TradingRepository _tradingRepository;

        [SetUp]
        public void Setup()
        {
            // Initialize the database connection
            _databaseConnection = new DatabaseConnection();
            _tradingRepository = new TradingRepository(_databaseConnection);
        }

        [Test]
        public void GetTradingDealRepository_ValidUsername_ReturnsDeals()
        {
            // Arrange
            var username = "testuser";

            // Act
            var result = _tradingRepository.GetTradingDealRepository(username);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);  // Check that the result is not empty
            Assert.That(result[0].Type, Is.EqualTo("monster"));  // Example of additional assertion
        }
        

        [Test]
        public void CheckCardIfUserNotOwnOrInDeckRepo_CardNotOwned_ReturnsFalse()
        {
            // Arrange
            var tradingDeal = new TradingDeal
            {
                Id = new Guid("417c14a0-b9d9-4a3e-b69d-1f7f7f8e4e50"),
                CardToTrade = new Guid("02a9c76e-b17d-427f-9240-2dd49b0d3bfd"),
                Type = "monster",
                MinimumDamage = 11
            };
            var username = "testuser";
            

            // Act
            var result = _tradingRepository.CheckCardIfUserNotOwnOrInDeckRepo(tradingDeal, username);

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CheckDealIsAlreadyExistRepo_DealExists_ReturnsTrue()
        {
            // Arrange
            var tradingDeal = new TradingDeal
            {
                Id = new Guid("417c14a0-b9d9-4a3e-b69d-1f7f7f8e4e50"),
                CardToTrade = new Guid("27051a20-8580-43ff-a473-e986b52f297a"),
                Type = "monster",
                MinimumDamage = 11
            }; // Initialize with test data
            var username = "testuser";
            

            // Act
            var result = _tradingRepository.CheckDealIsAlreadyExistRepo(tradingDeal, username);

            // Assert
            Assert.IsTrue(result);
        }

        // ... Additional tests for CheckDealIsAlreadyExistRepo
        [Test]
        public void CheckDealIsAlreadyExistRepo_DealNotExists_ReturnsFalse()
        {
            // Arrange
            var tradingDeal = new TradingDeal
            {
                Id = new Guid("417c14a0-b9d9-4a3e-b69d-1f7f7f8e4e50"),
                CardToTrade = new Guid("02a9c76e-b17d-427f-9240-2dd49b0d3bfd"),
                Type = "monster",
                MinimumDamage = 11
            }; // Initialize with test data
            var username = "testuser";
            

            // Act
            var result = _tradingRepository.CheckDealIsAlreadyExistRepo(tradingDeal, username);

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateTradingRepo_ValidTradingDeal_CreatesDeal()
        {
            // Arrange
            var tradingDeal = new TradingDeal
            {
                Id = new Guid("0536714c-018e-44da-8747-d1d06dde73ae"),
                CardToTrade = new Guid("166c1fd5-4dcb-41a8-91cb-f45dcd57cef3"),
                Type = "monster",
                MinimumDamage = 22
            }; // Initialize with test data
            

            // Act
            TestDelegate testAction = () => _tradingRepository.CreateTradingRepo(tradingDeal);

            // Assert
            Assert.DoesNotThrow(testAction);
            _tradingRepository.DeleteTradingRepo(tradingDeal.Id);
        }
        


        [Test]
        public void UserHasDealRepository_ValidInput_ReturnsExpectedResult()
        {
            // Arrange
            var tradingDeal = new TradingDeal
            {
                Id = new Guid("b8ba33ec-b1b7-449f-b7ed-044b0892f95e"),
                CardToTrade = new Guid("166c1fd5-4dcb-41a8-91cb-f45dcd57cef3"), // Intentionally wrong GUID
                Type = "monster",
                MinimumDamage = 22
            };
            var tradingId = tradingDeal.Id;
            var username = "testuser";
            _tradingRepository.CreateTradingRepo(tradingDeal);

            // Act
            var result = _tradingRepository.UserHasDealRepository(tradingId, username);

            // Assert
            Assert.IsTrue(result);
            _tradingRepository.DeleteTradingRepo(tradingId);
        }

        

        [Test]
        public void FindDealRepo_ValidTradingId_ReturnsTrueIfDealExists()
        {
            // Arrange
            // Arrange
            var tradingDeal = new TradingDeal
            {
                Id = new Guid("cd6ead20-6c87-4459-a244-c8d0f7dece40"),
                CardToTrade = new Guid("2508bf5c-20d7-43b4-8c77-bc677decadef"), // Intentionally wrong GUID
                Type = "monster",
                MinimumDamage = 22
            };
            var tradingId = tradingDeal.Id;
            var username = "testuser";
            _tradingRepository.CreateTradingRepo(tradingDeal);

            // Act
            var result = _tradingRepository.FindDealRepo(tradingId);

            // Assert
            Assert.IsTrue(result); // or Assert.IsFalse(result) based on the setup
            _tradingRepository.DeleteTradingRepo(tradingId);
        }
        

        [Test]
        public void DeleteTradingRepo_ValidTradingId_DeletesDeal()
        {
            // Arrange
            // Arrange
            var tradingDeal = new TradingDeal
            {
                Id = new Guid("2453966d-8a2b-465a-a1e7-57698ba7b22c"),
                CardToTrade = new Guid("166c1fd5-4dcb-41a8-91cb-f45dcd57cef3"), // Intentionally wrong GUID
                Type = "monster",
                MinimumDamage = 22
            };
            var tradingId = tradingDeal.Id;
            var username = "testuser";
            _tradingRepository.CreateTradingRepo(tradingDeal);

            // Act
            TestDelegate testAction = () => _tradingRepository.DeleteTradingRepo(tradingId);

            // Assert
            Assert.DoesNotThrow(testAction);
        }

        // ... Additional tests for DeleteTradingRepo

        // Continue writing tests for each method following this pattern

        // Don't forget to test exception handling and edge cases
    }
}