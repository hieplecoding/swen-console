using MonsterTradingCards.Database;
using MonsterTradingCards.Models;
using MonsterTradingCards.Repositories;
using MonsterTradingCards.Services;
using TestUser.Utilities;

namespace TestUser;

public class DeskTest
{
    private DatabaseConnection _databaseConnection;

    private DeskService _deskService;
    private DeskRepository _deskRepository;

    private UserService _userService;
    private UserRepository _userRepository;

    [SetUp]
    public void Setup()
    {
        // Initialize the data connection
        _databaseConnection = new DatabaseConnection();


        // Initialize for package 
        _deskRepository = new DeskRepository(_databaseConnection);
        _userRepository = new UserRepository(_databaseConnection);
        _deskService = new DeskService(_deskRepository, _userRepository);
    }

    [Test]
    public void CheckIfUserHasCardInDesk_RealUser()
    {
        // Arrange
        var realUsername = "kienboec"; // Replace with a username that has cards in their desk
        var knownCardIds = new List<Guid>
        {
            Guid.Parse("845f0dc7-37d0-426e-994e-43fc3ac83c08"),
            Guid.Parse("99f8f8dc-e25e-4a95-aa2c-782823f36e2a"),
            Guid.Parse("e85e3976-7c86-4d06-9a80-641c2019a79f"),
            Guid.Parse("171f6076-4eb5-4a7d-b3f2-2d650cc3d237")
        };

        // Act
        bool result = _deskRepository.CheckIfUserHasCardInDeskRepo(realUsername, knownCardIds);

        // Assert
        Assert.IsTrue(result, "Real users should have these cards in their desk.");
    }

    [Test]
    public void CheckIfUserHasCardInDesk_TestUser()
    {
        // Arrange
        var helper = new HelperMethod();
        var testUsername = "testuser5";
        var testPassword = "123456";
        helper.AddTestUserToDatabase(testUsername, testPassword); // Add a test user

        var testCardIds = new List<Guid>
        {
            Guid.Parse("e47d6b02-522d-4697-ab54-8b929368751e"),
            Guid.Parse("0c15af57-579b-453e-94fd-e1161c29bd0e"),
            Guid.Parse("b0a72ca3-0e42-4aa4-adeb-ed72ee265748"),
            Guid.Parse("f461bd86-e163-4192-a520-a66f7422f90d")
        };


        // Act
        bool result = _deskRepository.CheckIfUserHasCardInDeskRepo(testUsername, testCardIds);

        // Assert
        Assert.IsFalse(result, "Test users should not have any cards in their desk.");

        // Clean up
        helper.DeleteTestUserFromDatabase(testUsername); // Delete the test user
    }
    [Test]
    public void GetCardInDesk_RealUser()
    {
        // Arrange
        var realUsername = "kienboec"; // Replace with a real username

        // Act
        var cardIds = _deskRepository.GetCardInDeskByUsernameRepo(realUsername);

        // Assert
        Assert.IsNotNull(cardIds, "The list of card IDs should not be null.");
        Assert.IsNotEmpty(cardIds, "Real users should have cards in their desk.");
        // You can also add more specific checks based on expected card IDs
    }

    [Test]
    public void GetCardInDesk_TestUser()
    {
        // Arrange
        var helper = new HelperMethod();
        var testUsername = "testuser5";
        var testPassword = "123456";
        helper.AddTestUserToDatabase(testUsername, testPassword); // Add a test user

        // Act
        var cardIds = _deskRepository.GetCardInDeskByUsernameRepo(testUsername);

        // Assert
        Assert.IsNotNull(cardIds, "The list of card IDs should not be null.");
        Assert.IsEmpty(cardIds, "Test users should not have any cards in their desk.");

        // Clean up
        helper.DeleteTestUserFromDatabase(testUsername); // Delete the test user
    }

}