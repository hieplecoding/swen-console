using MonsterTradingCards.Database;
using MonsterTradingCards.Repositories;
using MonsterTradingCards.Services;
using MonterTradingCards.Models;
using TestUser.Utilities;
using Npgsql;

namespace TestUser
{
    public class Tests
    {
        private UserService _userService;
        private UserRepository _userRepository;
        private DatabaseConnection _databaseConnection;

        [SetUp]
        public void Setup()
        {
            // Initialize the database connection
            _databaseConnection = new DatabaseConnection();

            // Initialize the repository with the database connection
            _userRepository = new UserRepository(_databaseConnection);

            // Initialize the service with the repository
            _userService = new UserService(_userRepository);
        }

        [Test]
        public void RegisterUser_ShouldSucceedWithNewUser()
        {
            // Arrange
            var expectedUsername = "testuser3";
            var exceptedPassword = "12345";
            //DeleteTestUserFromDatabase(expectedUsername); // Ensure clean state

            var helper = new HelperMethod();
            helper.DeleteTestUserFromDatabase(expectedUsername);
            
            var newUser = new UserCredentials { Username = expectedUsername, Password = exceptedPassword };
            // Act
            bool result = _userService.RegisterUser(newUser);

            // Assert
            Assert.IsTrue(result, "User registration should succeed for a new user.");
        }

        [Test]
        public void GetUserByUsername_ShouldSucceedWithTestUser()
        {
            // Arrange
            var expectedUsername = "testuser3";
            var exceptedPassword = "12345";
            //DeleteTestUserFromDatabase(expectedUsername);// Ensure clean state
            var helper = new HelperMethod();
            helper.DeleteTestUserFromDatabase(expectedUsername);
            helper.AddTestUserToDatabase(expectedUsername, exceptedPassword);
            
            //AddTestUserToDatabase(expectedUsername, exceptedPassword);
            var result = _userRepository.GetUserByUsername(expectedUsername);
            // Assert
            // Assert
            Assert.IsNotNull(result, "The result should not be null for an existing user.");
            Assert.AreEqual(expectedUsername, result.Username, "The username should match the expected value.");

        }

        [Test]
        public void LoginUser_ShouldSucceedWithTestUsers()
        {
            // Arrange
            var expectedUsername = "testuser3";
            var exceptedPassword = "12345";
            
            var helper = new HelperMethod();
            helper.DeleteTestUserFromDatabase(expectedUsername);
            helper.AddTestUserToDatabase(expectedUsername, exceptedPassword);
            // Act
            
            bool result = _userRepository.LoginUser(expectedUsername, exceptedPassword);
            
            // Assert 
            Assert.IsTrue(result, "Login should be successful with correct username and password.");
        }
        
        [Test]
        public void GetCardInDeskByUsernameRepo_WithCards_ReturnsCardIds()
        {
            // Arrange
            var username = "kienboec"; // Replace with a valid username
            // Setup your database connection and commands to return expected results

            // Act
            var result = _userRepository.GetCardInDeskByUsernameRepo(username);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result); // Assert that the result is not empty
            // Additional assertions to verify that the returned card IDs are correct
        }

        
    }
}
