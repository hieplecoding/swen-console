using MonsterTradingCards.Database;
using MonsterTradingCards.Models;
using MonsterTradingCards.Repositories;
using MonsterTradingCards.Services;
using TestUser.Utilities;
namespace TestUser;

public class CardTest
{
    private  DatabaseConnection _databaseConnection;

    private CardService _cardService;
    private CardRepository _cardRepository;
        
    [SetUp]
    public void Setup()
    {
                
        // Initialize the data connection
        _databaseConnection = new DatabaseConnection();
                
                
                
                
                
        // Initialize for package 
        _cardRepository = new CardRepository(_databaseConnection);
        _cardService = new CardService(_cardRepository);
    }

    // one test for new user: testuser and one test for normal user:
    // should return count = 0 for testuser and return > 0 for normal user
    
    [Test]
    public void GetCardsForNormalUser()
    {
        // Arrange
        var normalUsername = "kienboec"; // Replace with a username that exists in your test DB and has cards

        // Act
        var cards = _cardRepository.GetCardsByUsername(normalUsername);

        // Assert
        Assert.IsTrue(cards.Count > 0, "Normal users should have one or more cards.");
    }

    [Test]
    public void GetCardsForTestUser()
    {
        // Arrange
        var helper = new HelperMethod();
        var testUsername = "testuser5";
        var testPassword = "1234";
        helper.AddTestUserToDatabase(testUsername, testPassword); // Add a test user

        // Act
        var cards = _cardRepository.GetCardsByUsername(testUsername);

        // Assert
        Assert.AreEqual(0, cards.Count, "Test users should have no cards.");

        // Clean up
        helper.DeleteTestUserFromDatabase(testUsername); // Delete the test user
    }

}