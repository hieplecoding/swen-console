using NUnit.Framework;
using Moq;
using MonsterTradingCards.Controllers;
using MonsterTradingCards.Interface;
using MonsterTradingCards.Interface.Battles;
using MonsterTradingCards.Services;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;
using MonsterTradingCards.Http; // Assuming this is your custom class
using System.Net.Sockets;
using System.Threading.Tasks;
using MonsterTradingCards.Database;
using MonsterTradingCards.Interface.Database;
using MonsterTradingCards.Repositories;
using MonterTradingCards.Models;
using Npgsql;
using System.Data.Common;

namespace TestUser
{
    public class BattlesTest
    {
        private Mock<IUserService> _mockUserService;
        private Mock<IBattlesService> _mockBattlesService;
        private BattlesController _controller;
        private BattlesService _battlesService;
        private Mock<IBattlesRepository> _mockBattlesRepository;
        private Mock<IDatabaseConnection> _mockDbConnection;
        private BattlesRepository _battlesRepository;

        [SetUp]
        public void Setup()
        {
            // Setup for BattlesController
            _mockUserService = new Mock<IUserService>();
            _mockBattlesService = new Mock<IBattlesService>();
            _controller = new BattlesController(_mockUserService.Object, _mockBattlesService.Object);

            // Setup for BattlesService
            _mockBattlesRepository = new Mock<IBattlesRepository>();
            _battlesService = new BattlesService(_mockBattlesRepository.Object);

            // Setup for BattlesRepository
            _mockDbConnection = new Mock<IDatabaseConnection>();
            _battlesRepository = new BattlesRepository(_mockDbConnection.Object);
        }

        // Tests for BattlesController...

        // Tests for BattlesService...
        [Test]
        public async Task EnterLobbyAsync_FirstUser_WaitsForSecondUser()
        {
            var user = new User("Player1", new Card?[4], new UserStats());
            var tcpClient = new TcpClient();

            var result = await _battlesService.EnterLobbyAsync(user, tcpClient);

            Assert.AreEqual("Player 1 Entered\nWaiting for player 2 to join...\n", result);
        }

        // Tests for BattlesRepository...
        /*
        [Test]
        public void ChangeStatsAfterBattle_ExecutesCommandsCorrectly()
        {
            // Arrange
            string userWin = "winnerUsername";
            string userLose = "loserUsername";

            var mockNpgsqlConnection = new Mock<NpgsqlConnection>();
            var mockNpgsqlCommand = new Mock<NpgsqlCommand>();
            mockNpgsqlCommand.Setup(m => m.ExecuteNonQuery()).Verifiable();
            mockNpgsqlConnection.Setup(conn => conn.CreateCommand()).Returns(mockNpgsqlCommand.Object);

            _mockDbConnection.Setup(db => db.GetConnection()).Returns(mockNpgsqlConnection.Object);

            // Act
            _battlesRepository.ChangeStatsAfterBattle(userWin, userLose);

            // Assert
            mockNpgsqlCommand.Verify(cmd => cmd.ExecuteNonQuery(), Times.Exactly(2));
            mockNpgsqlCommand.VerifySet(cmd => cmd.CommandText = It.IsAny<string>(), Times.Exactly(2));
            _mockDbConnection.Verify(db => db.GetConnection(), Times.AtLeastOnce());
        }
        */
    }
}
