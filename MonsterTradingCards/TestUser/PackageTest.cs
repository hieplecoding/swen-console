using MonsterTradingCards.Database;
using MonsterTradingCards.Models;
using MonsterTradingCards.Models.ModelsForGame;
using MonsterTradingCards.Repositories;
using MonsterTradingCards.Services;
using MonterTradingCards.Models;
using Npgsql;

namespace TestUser;

[TestFixture]
public class PackageTest
{
                          
    
            
            private  DatabaseConnection _databaseConnection;

            private PackageService _packageService;
            private PackageRepository _packageRepository;
        
            [SetUp]
            public void Setup()
            {
                
                // Initialize the data connection
                _databaseConnection = new DatabaseConnection();
                
                
                
                
                
                // Initialize for package 
                _packageRepository = new PackageRepository(_databaseConnection);
                _packageService = new PackageService(_packageRepository);
            }
            
            // create package with a empty card, return false
            [Test]
            public void CreatePackage_EmptyCardList_ReturnsFalse()
            {
                var result = _packageService.CreatePackage(new List<Card>());
                Assert.IsFalse(result);
            }
            
            [Test]
            public void AddCards_WithValidCardList_AddsCardsToNewPackage()
            {
                // Arrange
                var cards = new List<Card>
                {
                    new Card(Guid.NewGuid(), "Card1", 10), // Create Card with Id, Name, and Damage
                    new Card(Guid.NewGuid(), "Card2", 15)
                };

                // Act
                var result = _packageRepository.AddCards(cards);

                // Assert
                Assert.IsTrue(result);
                // Additional assertions to check if cards are correctly added to the database
            }
            
            [Test]
            public void AddPackageToUser_WithValidUsername_AssignsPackage()
            {
                // Arrange
                var username = "admin"; // Replace with a valid username

                // Act
                var result = _packageRepository.AddPackageToUser(username);

                // Assert
                Assert.IsTrue(result);
                // Additional assertions to check if the package is assigned and coins are deducted
            }

            [Test]
            public void CheckMoney_WithSufficientCoins_ReturnsTrue()
            {
                // Arrange
                var username = "admin"; // Replace with a username that has sufficient coins

                // Act
                var result = _packageRepository.checkMoney(username);

                // Assert
                Assert.IsTrue(result);
            }

}